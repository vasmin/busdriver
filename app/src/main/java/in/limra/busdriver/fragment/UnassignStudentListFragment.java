package in.limra.busdriver.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.adapter.AssignStudentAdapter;
import in.limra.busdriver.adapter.UnassignStudentAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.RealmHelper;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.Response;
import in.limra.busdriver.response.loginresponse.studentlistresponse.StudentListResponseFromPickUp;
import in.limra.busdriver.response.loginresponse.studentlistresponse.Student_List_From_PickUp;
import in.limra.busdriver.response.loginresponse.unassignresponse.UnassignResponse;
import in.limra.busdriver.response.loginresponse.unassignresponse.UnassignedStudentList;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class UnassignStudentListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    String stopName;
    String pointId;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayoutManager linearLayoutManager;
    UnassignStudentAdapter adapter;

    @BindView(R.id.search_view)
    SearchView searchView;
    RealmResults<UnassignedStudentList> unassignedStudentLists;
    Realm realm;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            stopName = getArguments().getString(Constants.STOPNAME);
            pointId = getArguments().getString(Constants.POINT_ID);
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    public static UnassignStudentListFragment newInstance(String param1,String pointId) {
        UnassignStudentListFragment fragment = new UnassignStudentListFragment();
        Bundle args = new Bundle();
        args.putString(Constants.STOPNAME, param1);
        args.putString(Constants.POINT_ID, pointId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_unassign_student, container, false);
        ButterKnife.bind(this,view);

        realm= RealmHelper.getRealmInstance();
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);


        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new UnassignStudentAdapter(getContext(), new UnassignStudentAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(UnassignedStudentList d, View view) throws ParseException {
                new AlertDialog.Builder(getActivity())
                        .setIcon(R.drawable.logo)
                        .setTitle("Assign Student")
                        .setMessage("Are you sure you want to assign student?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                assignedStudent(d.getStudentid());
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        unassignedStudentLists=realm.where(UnassignedStudentList.class).findAllAsync();
        unassignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<UnassignedStudentList>>() {
            @Override
            public void onChange(RealmResults<UnassignedStudentList> unassignedStudentList) {

                if(unassignedStudentLists.size()>0){
                    adapter.setData(unassignedStudentLists);
                    recyclerView.setAdapter(adapter);
                }

                if(unassignedStudentLists.size()==0){
                    data.setVisibility(View.VISIBLE);
                }else{
                    data.setVisibility(View.GONE);
                }

            }
        });

        searchView.clearFocus();
        searchView.setQueryHint("Search student");
        searchView.setFocusable(false);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query!=null && !query.equals("")) {
                   unassignedStudentLists=realm.where(UnassignedStudentList.class)
                            .or().contains("studentName", query, Case.INSENSITIVE)
                            .or().contains("address", query, Case.INSENSITIVE)
                            .or().contains("batch", query, Case.INSENSITIVE)
                            .or().contains("status", query, Case.INSENSITIVE)
                            .findAllAsync();
                    unassignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<UnassignedStudentList>>() {
                        @Override
                        public void onChange(RealmResults<UnassignedStudentList> unassignedStudentList) {
                            if(unassignedStudentLists.size()>0){
                                adapter.setData(unassignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(unassignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    unassignedStudentLists=realm.where(UnassignedStudentList.class).findAllAsync();
                    unassignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<UnassignedStudentList>>() {
                        @Override
                        public void onChange(RealmResults<UnassignedStudentList> unassignedStudentList) {
                            if(unassignedStudentLists.size()>0){
                                adapter.setData(unassignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(unassignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);
                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(query!=null && !query.equals("")) {
                    unassignedStudentLists=realm.where(UnassignedStudentList.class)
                            .or().contains("studentName", query, Case.INSENSITIVE)
                            .or().contains("address", query, Case.INSENSITIVE)
                            .or().contains("batch", query, Case.INSENSITIVE)
                            .or().contains("status", query, Case.INSENSITIVE)
                            .findAllAsync();
                    unassignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<UnassignedStudentList>>() {
                        @Override
                        public void onChange(RealmResults<UnassignedStudentList> unassignedStudentList) {
                            if(unassignedStudentLists.size()>0){
                                adapter.setData(unassignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(unassignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    unassignedStudentLists=realm.where(UnassignedStudentList.class).findAllAsync();
                    unassignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<UnassignedStudentList>>() {
                        @Override
                        public void onChange(RealmResults<UnassignedStudentList> unassignedStudentList) {
                            if(unassignedStudentList.size()>0){
                                adapter.setData(unassignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(unassignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                return false;
            }
        });


        unAssignedStudentList();
        return view;
    }





    public void assignedStudent(String studentId){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("point_id",pointId);
        hashMap.put("student_id",studentId);
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));
        Call<in.limra.busdriver.response.loginresponse.Response> call=RestClient.get().AssignedStudent(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.limra.busdriver.response.loginresponse.Response>() {
            @Override
            public void onResponse(Call<in.limra.busdriver.response.loginresponse.Response> call, retrofit2.Response<Response> response) {
                in.limra.busdriver.response.loginresponse.Response response1=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(response1.getStatus()){
                        AppUtils.snackBar(getActivity(),response1.getMessage());
                        unAssignedStudentList();
                    }else{
                        AppUtils.snackBar(getActivity(),response1.getMessage());
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<in.limra.busdriver.response.loginresponse.Response> call, Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                AppUtils.errorSnackBar(getActivity(),Constants.SERVERTIMEOUT);

            }
        });
    }

    public void unAssignedStudentList(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("trip_id", SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));
        hashMap.put("stop_name",stopName);
        Call<UnassignResponse> call= RestClient.get().unAssignedStudentList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<UnassignResponse>() {
            @Override
            public void onResponse(Call<UnassignResponse> call, retrofit2.Response<UnassignResponse> response) {
                UnassignResponse unassignedStudentList=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(unassignedStudentList.getStatus()){

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(UnassignedStudentList.class);
                                realm.copyToRealmOrUpdate(unassignedStudentList.getUnassignedStudentList());
                            }
                        });

                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<UnassignResponse> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(getActivity(),Constants.SERVERTIMEOUT);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onRefresh() {
        unAssignedStudentList();
    }
}
