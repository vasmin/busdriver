package in.limra.busdriver.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.activity.StartTripActivity;
import in.limra.busdriver.adapter.StudentAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.RealmHelper;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.studentlistresponse.StudentList;
import in.limra.busdriver.response.loginresponse.studentlistresponse.StudentListResponse;
import in.limra.busdriver.response.loginresponse.studentlistresponse.Student_List_From_PickUp;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    StudentAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    Realm realm;


    @BindView(R.id.search_view)
    SearchView searchView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_student, container, false);
        ButterKnife.bind(this,view);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);


        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        realm = RealmHelper.getRealmInstance();




        adapter = new StudentAdapter(getContext(), new StudentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final StudentList d, View view) {
                final ImageView contactImage = view.findViewById(R.id.contact);

                contactImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!d.isView()) {
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    d.setView(true);
                                    realm.copyToRealmOrUpdate(d);

                                }
                            });
                            adapter.notifyDataSetChanged();

                        } else {
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    d.setView(false);
                                    realm.copyToRealmOrUpdate(d);
                                }
                            });
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        });

        searchView.setQueryHint("Search student");
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query!=null && !query.equals("")) {
                    RealmResults<StudentList> studentLists=realm.where(StudentList.class)
                            .or().contains("studentName", query, Case.INSENSITIVE)
                            .or().contains("address", query, Case.INSENSITIVE)
                            .or().contains("batch", query, Case.INSENSITIVE)
                            .or().contains("status", query, Case.INSENSITIVE)
                            .findAllAsync();
                    studentLists.addChangeListener(new RealmChangeListener<RealmResults<StudentList>>() {
                        @Override
                        public void onChange(RealmResults<StudentList> StudentLists) {
                            if(StudentLists.size()>0){
                                adapter.setData(StudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(StudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                else{
                    RealmResults<StudentList> studentLists=realm.where(StudentList.class)
                            .findAllAsync();
                    studentLists.addChangeListener(new RealmChangeListener<RealmResults<StudentList>>() {
                        @Override
                        public void onChange(RealmResults<StudentList> StudentLists) {
                            if(StudentLists.size()>0){
                                adapter.setData(StudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(StudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(query!=null && !query.equals("")) {
                    RealmResults<StudentList> studentLists=realm.where(StudentList.class)
                            .or().contains("studentName", query, Case.INSENSITIVE)
                            .or().contains("address", query, Case.INSENSITIVE)
                            .or().contains("batch", query, Case.INSENSITIVE)
                            .or().contains("status", query, Case.INSENSITIVE)
                            .findAllAsync();
                    studentLists.addChangeListener(new RealmChangeListener<RealmResults<StudentList>>() {
                        @Override
                        public void onChange(RealmResults<StudentList> StudentLists) {
                            if(StudentLists.size()>0){
                                adapter.setData(StudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(StudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    RealmResults<StudentList> studentLists=realm.where(StudentList.class)
                            .findAllAsync();
                    studentLists.addChangeListener(new RealmChangeListener<RealmResults<StudentList>>() {
                        @Override
                        public void onChange(RealmResults<StudentList> StudentLists) {
                            if(StudentLists.size()>0){
                                adapter.setData(StudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(StudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                return false;
            }
        });

        RealmResults<StudentList> studentLists=realm.where(StudentList.class)
                .findAllAsync();
        studentLists.addChangeListener(new RealmChangeListener<RealmResults<StudentList>>() {
            @Override
            public void onChange(RealmResults<StudentList> StudentLists) {
                if(StudentLists.size()>0){
                    adapter.setData(StudentLists);
                    recyclerView.setAdapter(adapter);
                }

                if(StudentLists.size()==0){
                    data.setVisibility(View.VISIBLE);

                }else{
                    data.setVisibility(View.GONE);
                }
            }
        });

        getStudentList();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getStudentList() {
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
         hashMap.put("trip_id", SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));
        Call<StudentListResponse> call= RestClient.get().getStudentList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<StudentListResponse>() {
            @Override
            public void onResponse(Call<StudentListResponse> call, Response<StudentListResponse> response) {
                StudentListResponse studentListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(studentListResponse.getStatus()){

                        if(studentListResponse.getStudentList()!=null) {

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(StudentList.class);
                                    realm.copyToRealmOrUpdate(studentListResponse.getStudentList());
                                }
                            });

                        }else{
                            AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                        }

                    }else{
                        if(studentListResponse.getStudentList()!=null) {
                            adapter.setData(studentListResponse.getStudentList());
                            recyclerView.setAdapter(adapter);
                            if (studentListResponse.getStudentList().size() == 0) {
                                data.setVisibility(View.VISIBLE);
                            } else {
                                data.setVisibility(View.GONE);
                            }
                        }else{
                            AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                        }
                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);

                }
            }

            @Override
            public void onFailure(Call<StudentListResponse> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(getActivity(),Constants.SERVERTIMEOUT);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        getStudentList();
    }
}
