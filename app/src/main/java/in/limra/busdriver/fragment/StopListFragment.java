package in.limra.busdriver.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import customfont.MyEditText;
import in.limra.busdriver.R;
import in.limra.busdriver.activity.HomeActivity;
import in.limra.busdriver.activity.PointListActivity;
import in.limra.busdriver.adapter.SpinnerAdapter;
import in.limra.busdriver.adapter.TripAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.data.SpinnerModel;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.Response;
import in.limra.busdriver.response.loginresponse.batchresponse.BatchDetail;
import in.limra.busdriver.response.loginresponse.batchresponse.BatchListResponse;
import in.limra.busdriver.response.loginresponse.pointresponse.PickupPoint;
import in.limra.busdriver.response.loginresponse.pointresponse.PointResponse;
import in.limra.busdriver.service.GPSTracker;
import retrofit2.Call;
import retrofit2.Callback;

public class StopListFragment extends Fragment implements LocationListener,SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    SpinnerAdapter BatchAdapter;

    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.createstop)
    TextView createStop;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    TripAdapter adapter;

    Location location;

    List<SpinnerModel> batchList=new ArrayList<>();
    Spinner batchspinner;
    CardView pickcard;

    private GPSTracker gps;
    private String latitude="";
    private String longitude="";
    String batchId="0";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_status_pick_up_frg, container, false);
        ButterKnife.bind(this,view);


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        gps = new GPSTracker(getActivity());

        if (gps.canGetLocation()) {
            location = gps.getLocation();
            if (location != null) {

                latitude = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLongitude());

                SharedPreferenceHelper.getInstance(getContext()).putString(Constants.LATITUDE, latitude);
                SharedPreferenceHelper.getInstance(getContext()).putString(Constants.LONGITUDE, longitude);

            }
        } else {
        }

        createStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPickUpTrip();

            }
        });

        adapter=new TripAdapter(getActivity(), new TripAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(PickupPoint d, View view) throws ParseException {
                Intent intent=new Intent(getActivity(),PointListActivity.class);
                intent.putExtra(Constants.STOPNAME,d.getStop_name());
                intent.putExtra(Constants.POINT_ID,d.getPointId());
                startActivity(intent);
            }
        });


        BatchAdapter=new SpinnerAdapter(getActivity());

        getPickupPointList();
        return view;
    }

    public void getBatchList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("route_id",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.ROUTE_ID));
        Call<BatchListResponse> call= RestClient.get().getBatchList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<BatchListResponse>() {
            @Override
            public void onResponse(Call<BatchListResponse> call, retrofit2.Response<BatchListResponse> response) {
                BatchListResponse batchListResponse=response.body();
                if(response.code()==200){
                    if(batchListResponse.getStatus()){

                        batchList.clear();
                        SpinnerModel pick=new SpinnerModel();
                        pick.setId("0");
                        pick.setName("Select Batch");
                        batchList.add(pick);

                        for(int i=0;i<batchListResponse.getBatchDetails().size();i++){
                            BatchDetail batchDetail=batchListResponse.getBatchDetails().get(i);
                            SpinnerModel spinnerModel1=new SpinnerModel();
                            spinnerModel1.setId(batchDetail.getBatchId());
                            spinnerModel1.setName(batchDetail.getBatchName());

                            if(!batchList.contains(spinnerModel1)) {
                                batchList.add(spinnerModel1);
                            }
                        }

                        BatchAdapter.setData(batchList);
                        batchspinner.setAdapter(BatchAdapter);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }
            }

            @Override
            public void onFailure(Call<BatchListResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void createPickUpTrip(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.create_trip_layout, null);
        builder.setView(view);
        final AlertDialog dialog;
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);

        final MyEditText addressEdit = (MyEditText) view.findViewById(R.id.addressEdit);
        final MyEditText serialEdit = (MyEditText) view.findViewById(R.id.serialNum);
        Button saveBtn= (Button) view.findViewById(R.id.addBtn);

        batchspinner=view.findViewById(R.id.batchspinner);
        pickcard=view.findViewById(R.id.pickcard);

        batchspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                batchId=batchList.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(addressEdit.getText().toString())){
                    addressEdit.setError("Enter Stop ");
                    return;
                }

                if(TextUtils.isEmpty(serialEdit.getText().toString())){
                    serialEdit.setError("Enter Serial No ");
                    return;
                }

                if(batchId.equals("0")){
                    Toast.makeText(getActivity(), "Please select batch", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } catch(Exception ignored) {
                    ignored.printStackTrace();
                }


                if (!addressEdit.getText().toString().equals("") && !serialEdit.getText().toString().equals("")){

                    new AlertDialog.Builder(getActivity())
                            .setIcon(R.drawable.logo)
                            .setTitle("Create Stop")
                            .setMessage("Are you sure ? You want to create Stop!")
                            .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(location!=null) {
                                        createStop(addressEdit.getText().toString(), serialEdit.getText().toString());
                                    }else{
                                        Toast.makeText(getActivity(), "Please check Your Gps ", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
        getBatchList();
        if( SharedPreferenceHelper.getInstance(getActivity()).getString(Constants.BATCH_ID)!=null) {
            pickcard.setVisibility(View.GONE);
            if (!SharedPreferenceHelper.getInstance(getActivity()).getString(Constants.BATCH_ID).equals("")) {
                pickcard.setVisibility(View.GONE);
                batchId=SharedPreferenceHelper.getInstance(getActivity()).getString(Constants.BATCH_ID);
            } else {
                pickcard.setVisibility(View.VISIBLE);
            }
        }else{
            pickcard.setVisibility(View.GONE);
            batchId=SharedPreferenceHelper.getInstance(getActivity()).getString(Constants.BATCH_ID);
        }

    }
    private void createStop(String addressVal, String serialVal){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("lat", String.valueOf(location.getLatitude()));
        hashMap.put("long", String.valueOf(location.getLongitude()));
        hashMap.put("stop", AppUtils.getCompleteAddressString(getActivity(),location.getLatitude(),location.getLongitude()));
        hashMap.put("point_name", addressVal);
        hashMap.put("serial_number", serialVal);
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));
        hashMap.put("batch_id", batchId);
        Call<Response> call= RestClient.get().createPickupPoint(SharedPreferenceHelper.getInstance(getActivity()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){

                        getPickupPointList();



                    }else{
                        View parentLayout = getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    View parentLayout = getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }

    private void getPickupPointList(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        if(location!=null) {
            hashMap.put("lat", String.valueOf(latitude));
            hashMap.put("long", String.valueOf(longitude));
        }
        hashMap.put("type", "pick");
        hashMap.put("route_id", SharedPreferenceHelper.getInstance(getContext()).getString(Constants.ROUTE_ID));
        hashMap.put("batch_id", SharedPreferenceHelper.getInstance(getContext()).getString(Constants.BATCH_ID));
        hashMap.put("trip_id", SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));

        Call<PointResponse> call= RestClient.get().getPickupPointList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PointResponse>() {
            @Override
            public void onResponse(Call<PointResponse> call, retrofit2.Response<PointResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                PointResponse pointResponse=response.body();
                if(response.code()==200){
                    if(pointResponse.getStatus()){
                        if(pointResponse.getPickupPoints()!=null) {
                            adapter.setData(pointResponse.getPickupPoints());
                            recyclerView.setAdapter(adapter);
                            if (pointResponse.getPickupPoints().size() == 0) {
                                data.setVisibility(View.VISIBLE);
                            } else {
                                data.setVisibility(View.GONE);
                            }
                        }else{
                            data.setVisibility(View.VISIBLE);
                        }
                    }else{
                        if(pointResponse.getPickupPoints()!=null) {
                            adapter.setData(pointResponse.getPickupPoints());
                            recyclerView.setAdapter(adapter);
                            if (pointResponse.getPickupPoints().size() == 0) {
                                data.setVisibility(View.VISIBLE);
                            } else {
                                data.setVisibility(View.GONE);
                            }
                        }else{
                            data.setVisibility(View.VISIBLE);
                        }
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    View parentLayout = getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<PointResponse> call, Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onLocationChanged(Location loc) {
        location=loc;
        SharedPreferenceHelper.getInstance(getContext()).putString(Constants.LATITUDE, String.valueOf(location.getLatitude()));
        SharedPreferenceHelper.getInstance(getContext()).putString(Constants.LONGITUDE, String.valueOf(location.getLongitude()));
        getPickupPointList();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRefresh() {
        getPickupPointList();
    }
}
