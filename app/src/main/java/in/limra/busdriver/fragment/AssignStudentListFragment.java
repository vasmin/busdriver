package in.limra.busdriver.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.util.HashMap;
import java.util.stream.Stream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.limra.busdriver.R;
import in.limra.busdriver.activity.ConductorHomeActivity;
import in.limra.busdriver.adapter.AssignStudentAdapter;
import in.limra.busdriver.adapter.StudentAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.RealmHelper;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.pointresponse.PickupPoint;
import in.limra.busdriver.response.loginresponse.studentlistresponse.StudentListResponseFromPickUp;
import in.limra.busdriver.response.loginresponse.studentlistresponse.Student_List_From_PickUp;
import in.limra.busdriver.response.loginresponse.unassignresponse.UnassignedStudentList;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssignStudentListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    String stopName;
    String pointId;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;



    LinearLayoutManager linearLayoutManager;
    AssignStudentAdapter adapter;

    @BindView(R.id.search_view)
    SearchView searchView;
    RealmResults<Student_List_From_PickUp> assignedStudentLists;
    Realm realm;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            stopName = getArguments().getString(Constants.STOPNAME);
            pointId = getArguments().getString(Constants.POINT_ID);

        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        realm= RealmHelper.getRealmInstance();
    }


    public static AssignStudentListFragment newInstance(String param1,String pointId) {
        AssignStudentListFragment fragment = new AssignStudentListFragment();
        Bundle args = new Bundle();
        args.putString(Constants.STOPNAME, param1);
        args.putString(Constants.POINT_ID, pointId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_assign_student, container, false);
        ButterKnife.bind(this,view);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new AssignStudentAdapter(getContext(), new AssignStudentAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(Student_List_From_PickUp d, View view) throws ParseException {

                LinearLayout dropLinear= view.findViewById(R.id.drop);
                dropLinear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(getActivity())
                                .setIcon(R.drawable.logo)
                                .setTitle("Drop")
                                .setMessage("Are you sure you want to drop student?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStudentAttendance(d,"drop");
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                });


                LinearLayout pickLinear= view.findViewById(R.id.pick);
                pickLinear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(getActivity())
                                .setIcon(R.drawable.logo)
                                .setTitle("Pick")
                                .setMessage("Are you sure you want to pick student?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStudentAttendance(d,"pick");
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                });

                LinearLayout unassign= view.findViewById(R.id.unassign);
                unassign.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(getActivity())
                                .setIcon(R.drawable.logo)
                                .setTitle("Un assign")
                                .setMessage("Are you sure you want to un assign student?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        unassignedStudent(d.getStudentid());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                });




            }
        });
        searchView.clearFocus();
        searchView.setFocusable(false);
        searchView.setQueryHint("Search student");
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query!=null && !query.equals("")) {
                    RealmResults<Student_List_From_PickUp> assignedStudentLists=realm.where(Student_List_From_PickUp.class)
                            .or().contains("studentName", query, Case.INSENSITIVE)
                            .or().contains("address", query, Case.INSENSITIVE)
                            .or().contains("batchShift", query, Case.INSENSITIVE)
                            .or().contains("status", query, Case.INSENSITIVE)
                            .findAllAsync();
                    assignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<Student_List_From_PickUp>>() {
                        @Override
                        public void onChange(RealmResults<Student_List_From_PickUp> assignedStudentLists) {
                            if(assignedStudentLists.size()>0){
                                adapter.setData(assignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(assignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    RealmResults<Student_List_From_PickUp> assignedStudentLists=realm.where(Student_List_From_PickUp.class).findAllAsync();
                    assignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<Student_List_From_PickUp>>() {
                        @Override
                        public void onChange(RealmResults<Student_List_From_PickUp> assignedStudentLists) {
                            if(assignedStudentLists.size()>0){
                                adapter.setData(assignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }


                            if(assignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);
                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(query!=null && !query.equals("")) {
                    RealmResults<Student_List_From_PickUp> assignedStudentLists=realm.where(Student_List_From_PickUp.class)
                            .or().contains("studentName", query, Case.INSENSITIVE)
                            .or().contains("address", query, Case.INSENSITIVE)
                            .or().contains("batchShift", query, Case.INSENSITIVE)
                            .or().contains("status", query, Case.INSENSITIVE)
                            .findAllAsync();
                    assignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<Student_List_From_PickUp>>() {
                        @Override
                        public void onChange(RealmResults<Student_List_From_PickUp> assignedStudentLists) {
                            if(assignedStudentLists.size()>0){
                                adapter.setData(assignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }

                            if(assignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    RealmResults<Student_List_From_PickUp> assignedStudentLists=realm.where(Student_List_From_PickUp.class).findAllAsync();
                    assignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<Student_List_From_PickUp>>() {
                        @Override
                        public void onChange(RealmResults<Student_List_From_PickUp> assignedStudentLists) {
                            if(assignedStudentLists.size()>0){
                                adapter.setData(assignedStudentLists);
                                recyclerView.setAdapter(adapter);
                            }


                            if(assignedStudentLists.size()==0){
                                data.setVisibility(View.VISIBLE);

                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }
                return false;
            }
        });

        getStudentList();

        RealmResults<Student_List_From_PickUp> assignedStudentLists=realm.where(Student_List_From_PickUp.class).findAllAsync();
        assignedStudentLists.addChangeListener(new RealmChangeListener<RealmResults<Student_List_From_PickUp>>() {
            @Override
            public void onChange(RealmResults<Student_List_From_PickUp> assignedStudentLists) {
                if(assignedStudentLists.size()>0){
                    adapter.setData(assignedStudentLists);
                    recyclerView.setAdapter(adapter);
                }


                if(assignedStudentLists.size()==0){
                    data.setVisibility(View.VISIBLE);
                }else{
                    data.setVisibility(View.GONE);
                }
            }
        });
        return view;
    }


     @OnClick(R.id.floating_button)
     public void onClickFloatButton()
     {

         HashMap<String,String> hashMap = new HashMap<>();
         hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));
         hashMap.put("reach_point",pointId);

         Call<in.limra.busdriver.response.loginresponse.Response> call = RestClient.get().reachAtPoint(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);

         call.enqueue(new Callback<in.limra.busdriver.response.loginresponse.Response>() {
             @Override
             public void onResponse(Call<in.limra.busdriver.response.loginresponse.Response> call, Response<in.limra.busdriver.response.loginresponse.Response> response) {
                 in.limra.busdriver.response.loginresponse.Response response1=response.body();

                 if (response.code()==200) {

                     if (response1.getStatus()) {
                         Toast.makeText(getActivity(), "You are reached.", Toast.LENGTH_SHORT).show();

                     } else {
                         Toast.makeText(getActivity(), response1.getMessage().toString(), Toast.LENGTH_SHORT).show();
                     }
                 }else if(response.code()==401){
                     AppUtils.logout(getActivity());
                 }else{
                     AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                 }
             }

             @Override
             public void onFailure(Call<in.limra.busdriver.response.loginresponse.Response> call, Throwable t) {

                 Log.d("sfgf","shfh");
             }
         });
     }

    public void updateStudentAttendance(Student_List_From_PickUp d,String type){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("point_id",pointId);
        hashMap.put("type",type);
        hashMap.put("regi_id",d.getStudentUniqueId());
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));
        hashMap.put("bus_id",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.BUS_ID));
        hashMap.put("lat",type);
        hashMap.put("long",type);
        Call<in.limra.busdriver.response.loginresponse.Response> call=RestClient.get().updateStudentAttendance(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.limra.busdriver.response.loginresponse.Response>() {
            @Override
            public void onResponse(Call<in.limra.busdriver.response.loginresponse.Response> call, Response<in.limra.busdriver.response.loginresponse.Response> response) {
                in.limra.busdriver.response.loginresponse.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        AppUtils.snackBar(getActivity(),response1.getMessage());
                        getStudentList();
                    }else{
                        AppUtils.errorSnackBar(getActivity(),response1.getMessage());
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<in.limra.busdriver.response.loginresponse.Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(getActivity(),Constants.SERVERTIMEOUT);

            }
        });
    }


    public void unassignedStudent(String studentId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("point_id",pointId);
        hashMap.put("student_id",studentId);
        Call<in.limra.busdriver.response.loginresponse.Response> call=RestClient.get().unAssignedStudent(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.limra.busdriver.response.loginresponse.Response>() {
            @Override
            public void onResponse(Call<in.limra.busdriver.response.loginresponse.Response> call, Response<in.limra.busdriver.response.loginresponse.Response> response) {
                in.limra.busdriver.response.loginresponse.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        AppUtils.snackBar(getActivity(),response1.getMessage());
                        getStudentList();
                    }else{
                        AppUtils.snackBar(getActivity(),response1.getMessage());
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<in.limra.busdriver.response.loginresponse.Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(getActivity(),Constants.SERVERTIMEOUT);

            }
        });
    }

    public void getStudentList(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("trip_id", SharedPreferenceHelper.getInstance(getContext()).getString(Constants.TRIP_ID));
        hashMap.put("stop_name",stopName);
        Call<StudentListResponseFromPickUp> call= RestClient.get().getStudentListFromPickUp(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<StudentListResponseFromPickUp>() {
            @Override
            public void onResponse(Call<StudentListResponseFromPickUp> call, Response<StudentListResponseFromPickUp> response) {
                StudentListResponseFromPickUp studentListResponseFromPickUp=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(studentListResponseFromPickUp.getStatus()){

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(Student_List_From_PickUp.class);
                                realm.copyToRealmOrUpdate(studentListResponseFromPickUp.getStudentListFromPickUp());
                            }
                        });


                    }else {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(Student_List_From_PickUp.class);
                                realm.copyToRealmOrUpdate(studentListResponseFromPickUp.getStudentListFromPickUp());
                            }
                        });
                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    AppUtils.errorSnackBar(getActivity(),Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<StudentListResponseFromPickUp> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(getActivity(),Constants.SERVERTIMEOUT);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onRefresh() {
        getStudentList();
    }
}
