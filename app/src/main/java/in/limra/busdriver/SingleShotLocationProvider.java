package in.limra.busdriver;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.GeomagneticField;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.core.content.ContextCompat;

import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.service.GPSTracker;


public class SingleShotLocationProvider {


    Context context;
    GPSTracker gpsTracker;
    Location locationVal; // Location
    double latitudeVal, longitudeVal;
    protected LocationManager locationManager;
    boolean isNetworkEnabled;
    LocationCallBack callBack;
    float mDeclination;
    int locationMode = 0;

    public interface LocationCallBack {
        void onNewLocationAvailable(GPSCoordinates location);

    }


    public SingleShotLocationProvider(Context context){

        this.context = context;
    }

    public void requestSingleUpdate(final Context context, final LocationCallBack callback) {

        this.callBack = callback;
        getLoc();
    }

    public static class GPSCoordinates {
        public float longitude = -1;
        public float latitude = -1;

        public GPSCoordinates(float theLatitude, float theLongitude) {
            longitude = theLongitude;
            latitude = theLatitude;
        }

        public GPSCoordinates(double theLatitude, double theLongitude) {
            longitude = (float) theLongitude;
            latitude = (float) theLatitude;
        }
    }

    public Location getLoc(){
        locationManager= (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isNetworkEnabled) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
            }
            locationManager.requestSingleUpdate(criteria, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                    if(location!=null){
                        locationVal =location;
                        if(SharedPreferenceHelper.getInstance(context).isTracking()) {

                            // String address= AppUtils.getCompleteAddressString(mContext,location.getLatitude(),location.getLongitude());
                            // saveLocation(location.getLatitude(), location.getLongitude(),address);
                            SharedPreferenceHelper.getInstance(context).putString(Constants.LATITUDE, String.valueOf(location.getLatitude()));
                            SharedPreferenceHelper.getInstance(context).putString(Constants.LONGITUDE, String.valueOf(location.getLongitude()));
                            SharedPreferenceHelper.getInstance(context).setLocationChanges(true);


                        }
                    }

                    GeomagneticField field = new GeomagneticField(
                            (float)location.getLatitude(),
                            (float)location.getLongitude(),
                            (float)location.getAltitude(),
                            System.currentTimeMillis()
                    );

                    // getDeclination returns degrees
                    mDeclination = field.getDeclination();
                    SharedPreferenceHelper.getInstance(context).putString(Constants.DECLINATION, String.valueOf(mDeclination));

                    latitudeVal = locationVal.getLatitude();
                    longitudeVal = locationVal.getLongitude();
                    //callBack.onNewLocationAvailable(new GPSCoordinates(latitudeVal, longitudeVal));
                    SharedPreferenceHelper.getInstance(context).putString(Constants.LATITUDE, String.valueOf(latitudeVal));
                    SharedPreferenceHelper.getInstance(context).putString(Constants.LONGITUDE, String.valueOf(longitudeVal));

                    //    gpsTracker.getLocation();

                }

                @Override
                public void onStatusChanged(String providers, int status, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            }, null);

        }else {
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled){
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                locationManager.requestSingleUpdate(criteria, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        if(location!=null){
                            locationVal =location;
                            if(SharedPreferenceHelper.getInstance(context).isTracking()) {

                                locationMode = Settings.Secure.LOCATION_MODE_SENSORS_ONLY;

                                // String address= AppUtils.getCompleteAddressString(mContext,location.getLatitude(),location.getLongitude());
                                // saveLocation(location.getLatitude(), location.getLongitude(),address);
                                SharedPreferenceHelper.getInstance(context).putString(Constants.LATITUDE, String.valueOf(location.getLatitude()));
                                SharedPreferenceHelper.getInstance(context).putString(Constants.LONGITUDE, String.valueOf(location.getLongitude()));
                                SharedPreferenceHelper.getInstance(context).setLocationChanges(true);

                            }
                        }

                        GeomagneticField field = new GeomagneticField(
                                (float)location.getLatitude(),
                                (float)location.getLongitude(),
                                (float)location.getAltitude(),
                                System.currentTimeMillis()
                        );

                        // getDeclination returns degrees
                        mDeclination = field.getDeclination();

                        SharedPreferenceHelper.getInstance(context).putString(Constants.DECLINATION, String.valueOf(mDeclination));

                        latitudeVal = location.getLatitude();
                        longitudeVal = location.getLongitude();
                        //callBack.onNewLocationAvailable(new GPSCoordinates(latitudeVal, longitudeVal));
                        SharedPreferenceHelper.getInstance(context).putString(Constants.LATITUDE, String.valueOf(latitudeVal));
                        SharedPreferenceHelper.getInstance(context).putString(Constants.LONGITUDE, String.valueOf(longitudeVal));
                        // gpsTracker.getLocation();
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {

                    }

                    @Override
                    public void onProviderDisabled(String s) {

                    }
                }, null);
            }
        }
        return locationVal;
    }



    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage("To continue, turn on device location, which uses Google's location service");

        // On pressing the Settings button.
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                String provider = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (!provider.contains("gps")) {
                    final Intent poke = new Intent();
                    poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                    poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                    poke.setData(Uri.parse("3"));
                    context.sendBroadcast(poke);
                }
            }
        });

        // On pressing the cancel button

        // Showing Alert Message
        alertDialog.show();
    }

}
