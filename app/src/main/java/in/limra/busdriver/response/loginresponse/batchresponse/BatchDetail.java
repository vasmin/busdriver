
package in.limra.busdriver.response.loginresponse.batchresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BatchDetail {

    @SerializedName("batch_id")
    @Expose
    private String batchId;
    @SerializedName("batch_name")
    @Expose
    private String batchName;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

}
