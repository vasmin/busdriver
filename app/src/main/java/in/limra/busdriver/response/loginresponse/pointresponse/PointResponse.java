
package in.limra.busdriver.response.loginresponse.pointresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PointResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("pickup_points")
    @Expose
    private List<PickupPoint> pickupPoints = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<PickupPoint> getPickupPoints() {
        return pickupPoints;
    }

    public void setPickupPoints(List<PickupPoint> pickupPoints) {
        this.pickupPoints = pickupPoints;
    }

}
