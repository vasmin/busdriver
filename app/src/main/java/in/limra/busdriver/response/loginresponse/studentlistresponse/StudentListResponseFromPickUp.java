package in.limra.busdriver.response.loginresponse.studentlistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StudentListResponseFromPickUp {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("student_list")
    @Expose
    private List<Student_List_From_PickUp> studentListFromPickUp = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Student_List_From_PickUp> getStudentListFromPickUp() {
        return studentListFromPickUp;
    }

    public void setStudentListFromPickUp(List<Student_List_From_PickUp> studentListFromPickUp) {
        this.studentListFromPickUp = studentListFromPickUp;
    }
}
