
package in.limra.busdriver.response.loginresponse.pointresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickupPoint {

    @SerializedName("stop")
    @Expose
    private String stop;

    @SerializedName("latitude")
    @Expose
    private float latVal;

    @SerializedName("longitude")
    @Expose
    private  float lanVal;

    @SerializedName("serial_number")
    @Expose
    private String sequenceNo;

    @SerializedName("point_id")
    @Expose
    private String pointId;

    @SerializedName("stop_name")
    @Expose
    private String stop_name;

    @SerializedName("number_student")
    @Expose
    private String totalStudent;


    @SerializedName("flag")
    @Expose
    private boolean flag;

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public float getLatVal() {
        return latVal;
    }

    public void setLatVal(float latVal) {
        this.latVal = latVal;
    }

    public float getLanVal() {
        return lanVal;
    }

    public void setLanVal(float lanVal) {
        this.lanVal = lanVal;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getStop_name() {
        return stop_name;
    }

    public void setStop_name(String stop_name) {
        this.stop_name = stop_name;
    }

    public String getTotalStudent() {
        return totalStudent;
    }

    public void setTotalStudent(String totalStudent) {
        this.totalStudent = totalStudent;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
