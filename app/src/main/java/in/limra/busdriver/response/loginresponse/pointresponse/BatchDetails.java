package in.limra.busdriver.response.loginresponse.pointresponse;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BatchDetails {

    @SerializedName("batch_id")
    @Expose
    private String batchId;

    @SerializedName("batch_name")
    @Expose
    private String batchName;

    @SerializedName("from_time")
    @Expose

    private String fromTime;

    @SerializedName("to_time")
    @Expose

    private String toTime;

  /*  @SerializedName("route_id")
    @Expose*/

    private String routeId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    @NonNull
    @Override
    public String toString() {
        return routeId.toString();
    }
}
