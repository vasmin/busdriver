
package in.limra.busdriver.response.loginresponse.busresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("bus_list")
    @Expose
    private List<BusList> busList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<BusList> getBusList() {
        return busList;
    }

    public void setBusList(List<BusList> busList) {
        this.busList = busList;
    }

}
