
package in.limra.busdriver.response.loginresponse.busresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusList {

    @SerializedName("bus_id")
    @Expose
    private String busId;
    @SerializedName("bus_number")
    @Expose
    private String busNumber;

    public String getBusId() {
        return busId;
    }

    public void setBusId(String busId) {
        this.busId = busId;
    }

    public String getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(String busNumber) {
        this.busNumber = busNumber;
    }

}
