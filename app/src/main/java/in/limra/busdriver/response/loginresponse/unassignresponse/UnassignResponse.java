
package in.limra.busdriver.response.loginresponse.unassignresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnassignResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("unassigned_student_list")
    @Expose
    private List<UnassignedStudentList> unassignedStudentList = null;
    @SerializedName("present")
    @Expose
    private Integer present;
    @SerializedName("absent")
    @Expose
    private Integer absent;
    @SerializedName("total")
    @Expose
    private Integer total;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<UnassignedStudentList> getUnassignedStudentList() {
        return unassignedStudentList;
    }

    public void setUnassignedStudentList(List<UnassignedStudentList> unassignedStudentList) {
        this.unassignedStudentList = unassignedStudentList;
    }

    public Integer getPresent() {
        return present;
    }

    public void setPresent(Integer present) {
        this.present = present;
    }

    public Integer getAbsent() {
        return absent;
    }

    public void setAbsent(Integer absent) {
        this.absent = absent;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
