
package in.limra.busdriver.response.loginresponse.studentlistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StudentListResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("student_list")
    @Expose
    private List<StudentList> studentList = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<StudentList> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<StudentList> studentList) {
        this.studentList = studentList;
    }

}
