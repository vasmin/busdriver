
package in.limra.busdriver.response.loginresponse.helpresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("help_contact_list")
    @Expose
    private List<HelpContactList> helpContactList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<HelpContactList> getHelpContactList() {
        return helpContactList;
    }

    public void setHelpContactList(List<HelpContactList> helpContactList) {
        this.helpContactList = helpContactList;
    }

}
