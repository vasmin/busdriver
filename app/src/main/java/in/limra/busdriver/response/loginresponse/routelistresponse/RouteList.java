
package in.limra.busdriver.response.loginresponse.routelistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteList {

    @SerializedName("route_id")
    @Expose
    private String routeId;
    @SerializedName("route_name")
    @Expose
    private String routeName;

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

}
