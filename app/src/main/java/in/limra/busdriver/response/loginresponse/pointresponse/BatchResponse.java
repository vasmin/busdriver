package in.limra.busdriver.response.loginresponse.pointresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BatchResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("batch")
    @Expose

    private List<BatchDetails> batchDetailsList;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<BatchDetails> getBatchDetailsList() {
        return batchDetailsList;
    }

    public void setBatchDetailsList(List<BatchDetails> batchDetailsList) {
        this.batchDetailsList = batchDetailsList;
    }
}
