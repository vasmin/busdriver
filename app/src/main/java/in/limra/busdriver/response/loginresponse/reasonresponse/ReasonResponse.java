
package in.limra.busdriver.response.loginresponse.reasonresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReasonResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("notification_reason_list")
    @Expose
    private List<NotificationReasonList> notificationReasonList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<NotificationReasonList> getNotificationReasonList() {
        return notificationReasonList;
    }

    public void setNotificationReasonList(List<NotificationReasonList> notificationReasonList) {
        this.notificationReasonList = notificationReasonList;
    }

}
