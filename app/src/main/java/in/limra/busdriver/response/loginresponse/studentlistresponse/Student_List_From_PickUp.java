package in.limra.busdriver.response.loginresponse.studentlistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Student_List_From_PickUp extends RealmObject {


    @PrimaryKey
    @SerializedName("studentid")
    @Expose
    private String studentid;

    @SerializedName("student_name")
    @Expose
    private String studentName;
    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("photo")
    @Expose
    private String imgUrl;

    @SerializedName("student_unique_id")
    @Expose
    private String studentUniqueId;

    @SerializedName("father_name")
    @Expose
    private String fatherName;
    @SerializedName("father_mobile_number")
    @Expose
    private String fatherMobileNumber;
    @SerializedName("mother_name")
    @Expose
    private String motherName;
    @SerializedName("mother_mobile_number")
    @Expose
    private String motherMobileNumber;

    @SerializedName("batch")
    @Expose
    private String batchShift;


    @SerializedName("type")
    @Expose
    private String type;


    @SerializedName("point_id")
    @Expose
    private String point_id;

    @SerializedName("session")
    @Expose
    private String session;


    @SerializedName("status")
    @Expose
    private String status;

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getStudentUniqueId() {
        return studentUniqueId;
    }

    public void setStudentUniqueId(String studentUniqueId) {
        this.studentUniqueId = studentUniqueId;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getFatherMobileNumber() {
        return fatherMobileNumber;
    }

    public void setFatherMobileNumber(String fatherMobileNumber) {
        this.fatherMobileNumber = fatherMobileNumber;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getMotherMobileNumber() {
        return motherMobileNumber;
    }

    public void setMotherMobileNumber(String motherMobileNumber) {
        this.motherMobileNumber = motherMobileNumber;
    }

    public String getBatchShift() {
        return batchShift;
    }

    public void setBatchShift(String batchShift) {
        this.batchShift = batchShift;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPoint_id() {
        return point_id;
    }

    public void setPoint_id(String point_id) {
        this.point_id = point_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
