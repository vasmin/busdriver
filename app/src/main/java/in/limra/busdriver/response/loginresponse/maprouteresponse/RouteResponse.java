
package in.limra.busdriver.response.loginresponse.maprouteresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("route_pickup_points")
    @Expose
    private List<RoutePickupPoint> routePickupPoints = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<RoutePickupPoint> getRoutePickupPoints() {
        return routePickupPoints;
    }

    public void setRoutePickupPoints(List<RoutePickupPoint> routePickupPoints) {
        this.routePickupPoints = routePickupPoints;
    }

}
