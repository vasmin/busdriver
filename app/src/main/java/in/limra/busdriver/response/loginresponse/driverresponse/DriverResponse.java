
package in.limra.busdriver.response.loginresponse.driverresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("driver_details")
    @Expose
    private DriverDetails driverDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public DriverDetails getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(DriverDetails driverDetails) {
        this.driverDetails = driverDetails;
    }

}
