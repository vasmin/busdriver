
package in.limra.busdriver.response.loginresponse.routelistresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("route_list")
    @Expose
    private List<RouteList> routeList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<RouteList> getRouteList() {
        return routeList;
    }

    public void setRouteList(List<RouteList> routeList) {
        this.routeList = routeList;
    }

}
