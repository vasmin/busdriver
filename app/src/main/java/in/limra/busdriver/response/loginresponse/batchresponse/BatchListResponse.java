
package in.limra.busdriver.response.loginresponse.batchresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BatchListResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("batch_details")
    @Expose
    private List<BatchDetail> batchDetails = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<BatchDetail> getBatchDetails() {
        return batchDetails;
    }

    public void setBatchDetails(List<BatchDetail> batchDetails) {
        this.batchDetails = batchDetails;
    }

}
