package in.limra.busdriver;

import android.app.Application;

import io.realm.Realm;


public class MyApplication extends Application {

    public static final String TAG = MyApplication.class.getSimpleName();
    private static MyApplication mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        mInstance = this;
        mInstance = this;
    }

}