package in.limra.busdriver.data;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.material.snackbar.Snackbar;
import com.orhanobut.dialogplus.DialogPlus;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.limra.busdriver.R;
import in.limra.busdriver.activity.HomeActivity;
import in.limra.busdriver.activity.LoginActivity;
import in.limra.busdriver.activity.SplashActivity;
import in.limra.busdriver.activity.StartTripActivity;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.Response;
import retrofit2.Call;
import retrofit2.Callback;


public class AppUtils {

    static DialogPlus dialog;
    static TextView decline,accept;
    static TextView serviceName,date;
    static TextView serviceAddress,serviceHours;
    static TextView serviceamt;
    static TextView duration;
    String Id="";
    private static final int REQUEST_PERMISSION = 123;
    public static String dateFormater(String dateFromJSON, String expectedFormat, String oldFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date date = null;
        String convertedDate = null;
        try {
            date = dateFormat.parse(dateFromJSON);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(expectedFormat);
            convertedDate = simpleDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static int numDaysBetween(final Calendar c, String start, String end) {
        long startDate = 0,endDate = 0;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date startdate = sdf.parse(start);
            Date enddate=sdf.parse(end);
            startDate = startdate.getTime();
            endDate = enddate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int result = 0;
        if (startDate <= endDate) {
            return result;
        }

        c.setTimeInMillis(startDate);
        final int toYear = c.get(Calendar.YEAR);
        result += c.get(Calendar.DAY_OF_YEAR);

        c.setTimeInMillis(endDate);
        result -= c.get(Calendar.DAY_OF_YEAR);

        while (c.get(Calendar.YEAR) < toYear) {
            result += c.getActualMaximum(Calendar.DAY_OF_YEAR);
            c.add(Calendar.YEAR, 1);
        }
        return result;
    }

    private static void checkNetworkConnection(Activity activity){
        AlertDialog.Builder builder =new AlertDialog.Builder(activity);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void errorSnackBar(Activity activity,String message){


        View parentLayout = activity.findViewById(android.R.id.content);
        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"#F44336\">"+message+"</font>"), Snackbar.LENGTH_LONG)
                .show();
    }

    public static void snackBar(Activity activity,String message){


        View parentLayout = activity.findViewById(android.R.id.content);
        Snackbar.make(parentLayout, Html.fromHtml(message), Snackbar.LENGTH_LONG)
                .show();
    }

    public static boolean isNetworkConnectionAvailable(Activity activity){
        ConnectivityManager cm =
                (ConnectivityManager)activity.getSystemService(activity.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            return true;
        }
        else{
            checkNetworkConnection(activity);
            Log.d("Network","Not Connected");
            return false;
        }
    }


    public static void logout(Activity activity) {
        if(SharedPreferenceHelper.getInstance(activity).isLoggerIn()) {
            SharedPreferenceHelper.getInstance(activity).setUserLoggedIn(false);
            SharedPreferenceHelper.getInstance(activity).setTracking(false);
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
            activity.finish();
        }
    }


    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

   /* public void logout(Activity activity){

        HashMap<String,String> hashMap=new HashMap<>();
        Call<Response> call= RestClient.get().logout(SharedPreferenceHelper.getInstance(activity).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        SharedPreferenceHelper.getInstance(activity).setUserLoggedIn(false);
                        Intent intent=new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);
                        activity.finish();
                    }else{
                        View parentLayout = activity.findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(activity.getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else{
                    View parentLayout = activity.findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMETHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(activity.getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = activity.findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(activity.getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }*/

    public static   boolean checkAndRequestPermissions(Activity activity) {
        int permissionSendMessage = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.SEND_SMS);
        int locationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_PERMISSION);
            return false;
        }
        return true;
    }

    public static  void startPickImageDialog(final int GALLERY_PICTURE, final int CAMERA_REQUEST, final String cameraPath, final Activity activity) {
        final AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                activity);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent pictureActionIntent;

                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        activity.startActivityForResult(
                                pictureActionIntent,
                                GALLERY_PICTURE);


                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface arg0, int arg1) {
//                        Intent i =new Intent(fragment.activity,MainCameraActivity.class);
//                        i.putExtra(Keys.Arguments.camera_path,cameraPath);
//                        fragment.startActivityForResult(i,CAMERA_REQUEST);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(activity.getApplicationContext(),activity.getPackageName()+".provider",new File(cameraPath)));

                        activity.startActivityForResult(intent,
                                CAMERA_REQUEST,new Bundle());


                    }
                });
        myAlertDialog.show();

    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    public static boolean isValidMail(String email, TextView txtEmail) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            txtEmail.setError("Not Valid Email");


        }
        return check;
    }

    public static boolean isValidMobile(String phone) {
        if (phone.length()!=10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }
    }


    public static String getFilePathFromContentUri(Uri selectedVideoUri,
                                                   ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }



    public static void grantUriPermission(Activity ctx, Intent intent, Uri uri, int permissionFlags) {
        List<ResolveInfo> resolvedIntentActivities = ctx.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
            String packageName = resolvedIntentInfo.activityInfo.packageName;

            ctx.grantUriPermission(packageName, uri, permissionFlags);
        }
    }





    public static  String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        if(geocoder!=null)
            try {
                List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
                if (addresses != null) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder("");

                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        return strAdd;
    }


}
