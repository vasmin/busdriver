package in.limra.busdriver.data;

/**
 * Created by bhushan on 11/26/2017.
 */

public class Constants {

    public static final String CATEGORYID = "categoryid";
    public static final String SKU = "sku";
    public static final String CARTID = "cartID";
    public static final String VISIBLE = "1";
    public static final String ORDERID = "orderid";
    public static final String LASTDATE = "lastdate";
    public static final String CURRENTINDEX = "currentIndex";
    public static final String CATEGORYID1 = "1";
    public static final String CATEGORYID2 = "2";
    public static final String CATEGORYID3 = "3";
    public static final String SOMETHING_WENT_WRONG = "Somthing went wrong from Server Side";
    public static final String NOTIFICATION = "notification";
    public static final String ACCOUNT_TYPE = "accounttype";
    public static final String SCHOOLID = "schoolID";
    public static final String QUESTIONID = "questionID";
    public static final String STUDENTNAME = "studentName";
    public static final String VENDORID = "vendorId";
    public static final String LEADID = "leadId";
    public static final String SERVICEID = "serviceId";
    public static final String CLOSEDLEAD = "closelead";
    public static final String TOTALLEAD = "totallead";
    public static final String USERID = "userid";
    public static final String FCMTOKEN = "fcmtoken";
    public static final String INVOICEID = "invoiceId";
    public static final String WORKINGLEAD = "workinglead";
    public static final String EARN = "earn";
    public static final String SERVERTIMEOUT ="Server Timeout" ;
    public static final String ROID = "roId";
    public static final String DEPOSIT = "deposit";
    public static final String RENT = "rent";
    public static final String MONTH = "month";
    public static final String MYRO = "myRo";
    public static final String DID = "did";
    public static final String REFERALCODE = "referalCode";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String COMPANYCODE = "companycode";
    public static final String SERVICELIST = "serviceList";
    public static final String PAYMENTSTATUS = "paymentStatus";
    public static final String VENDORAMT = "vamt";
    public static final String ENQUIRY = "enquiry";
    public static final String ISPOPUP = "isPop";
    public static final String BUY = "buy";
    public static final String BOOKINGTYPE = "bookingtype";
    public static final String JOBID = "jobId";
    public static final String VID = "vId";
    public static final String JOB = "job";
    public static final String TRIP_ID = "trip_id";
    public static final String USERTYPE = "usertype";
    public static final String STOPNAME = "stopname";
    public static String username = "username";
    public static String password = "password";
    public static String confirmpassword = "password_confirmation";
    public static String id = "id";
    public static String token = "token";
    public static String isLoggedIn = "isloggedin";
    public static String CompanyProfile = "companyProfile";
    public static String AuthToken = "AuthToken";
    public static String isLocationChanges="locationChanged";
    public static String isTracking="tracking";
    public static String DECLINATION="declination";


    public static String TESTID="testId";
    public static String TeacherName="teacherName";
    public static String imagePath="/HappyHelp/Images/";
    public static String isFirst="isFirst";
    public static String routeName="route";
    public static String ROUTE_ID="route_id";
    public static String BUS_ID="bus_id";
    public static String BATCH_ID="batch_id";
    public static String BATCH_ID_DROP="batch_id_drop";
    public static String BATCH_TYPE="batch_type";
    public static String POINT_ID="point_id";
    public static String isTripStart="tripStart";
}
