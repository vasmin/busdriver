package in.limra.busdriver.fcm;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.common.eventbus.EventBus;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import java.util.HashMap;

import in.limra.busdriver.R;
import in.limra.busdriver.activity.SplashActivity;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.RingtoneManager.getDefaultUri;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String serviceId="";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

       // sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("body"));

        Log.e(TAG, "From: " +remoteMessage.getFrom());
        Log.e(TAG, "Token: " + FirebaseInstanceId.getInstance().getToken());

        if (remoteMessage.getNotification() != null) {
            // do nothing if Notification message is received
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            String body = remoteMessage.getData().get("body");

        }

        if (remoteMessage.getData().size() > 0) {

            String message = "";
            String order_id = "";
            String type = "";
            String date = "";
            String orderStatus = "";
            String title = "";
            Object obj = remoteMessage.getData().get("serviceid");
            if (obj != null) {
                try {
                    order_id = obj.toString();
                    serviceId=order_id;
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
            Object obj2 = remoteMessage.getData().get("type");
            if (obj2 != null) {
                try {
                    type = obj2.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Object obj3 = remoteMessage.getData().get("appointmentAt");
            if (obj3 != null) {
                try {
                    date = obj3.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Object obj4 = remoteMessage.getData().get("message");
            if (obj4 != null) {
                try {
                    message = obj4.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Object obj5 = remoteMessage.getData().get("orderStatus");
            if (obj5 != null) {
                try {
                    orderStatus = obj5.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Object obj6 = remoteMessage.getData().get("title");
            if (obj6 != null) {
                try {
                    title = obj6.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                title = "Beautician";
            }

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if (!order_id.equals("")) {
                try {
                   // sendNotificationWithData(type, message, order_id, date, orderStatus,title);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//                else if(type != null && type.equals("SERVICE_UPDATE")){
//                    startService(new Intent(getApplicationContext(),ServiceFetchingService.class));
//                }
        } else {
            if (remoteMessage.getNotification() != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                showNotificationMessage(remoteMessage,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
            } else {
                sendNotification(remoteMessage,remoteMessage.getNotification().getTitle());
            }
        }else{

            }

        }

    }


  /*  private void sendNotification(String messageTitle, String messageBody) {
        Intent intent = new Intent(this, ServiceRequestActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0 *//* request code *//*, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long[] pattern = {500,500,500,500,500};

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.splash_logo)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setLights(Color.BLUE,1,1)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());
    }*/


    @Override
    public void onNewToken(String token) {
        Log.e(TAG, "Refreshed token: " + token);
        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.FCMTOKEN,token);

       // setfcmToken();

    }

 /*   public void setfcmToken(){
        HashMap<String, String> hashMap=new HashMap<>();
        hashMap.put("fcmtoken",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.FCMTOKEN));
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<Object> call= RestClient.get().updatefcm(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if(response.code()==200){

                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{

                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }*/


    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    private void sendNotification(RemoteMessage remoteMessage, String messageBody) {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        long notificatioId = System.currentTimeMillis();

        String click_action=remoteMessage.getNotification().getClickAction();
        Intent intent = new Intent(click_action);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent contentIntent = PendingIntent.getActivity(this, (int) (Math.random() * 100), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            currentapiVersion = R.drawable.logo;
        } else{
            currentapiVersion = R.drawable.logo;
        }
        Uri defaultSoundUri = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(currentapiVersion)
                .setContentTitle(this.getResources().getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{0, 500, 1000})
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setContentIntent(contentIntent);

        mNotificationManager.notify((int) notificatioId, notificationBuilder.build());
    }


    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showNotificationMessage(RemoteMessage remoteMessage, String message, String body) {
        String click_action=remoteMessage.getNotification().getClickAction();
        Intent intent = new Intent(click_action);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent((int) (Math.random() * 100), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        @SuppressLint("InlinedApi") int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);

            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }




        Uri defaultSoundUri = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(body)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setAutoCancel(true)
                .setVibrate(new long[]{0, 500, 1000})
                .setSound(defaultSoundUri)
                .setDefaults(Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);




        notificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());


    }
}