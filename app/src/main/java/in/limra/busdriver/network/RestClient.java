package in.limra.busdriver.network;
import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import in.limra.busdriver.RealmString;
import io.realm.RealmList;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by tushar on 12/2/17.
 */

public class RestClient {
    private static APIService REST_CLIENT = null;
    private static String BASE_URL = "https://digitaldatasystem.in/digital-data-system/TransportAPIs/";

    static {
        setupRestClient();
    }


    public static APIService get() {
        return REST_CLIENT;
    }

    public static APIService get(String baseUrl) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(interceptor);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(APIService.class);
    }

    private static void setupRestClient() {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(interceptor);

        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request request = chain.request();

                // try the request
                Response response = null;
                int tryCount = 1;
                while (tryCount <= 5) {
                    try {
                        response = chain.proceed(request);
                        break;
                    } catch (Exception e) {

                        if ("Canceled".equalsIgnoreCase(e.getMessage())) {
                            // Request canceled, do not retry
                            throw e;
                        }
                        if (tryCount >= 5) {
                            // max retry count reached, giving up
                            throw e;
                        }

                        try {
                            Thread.sleep(5 * tryCount);
                        } catch (InterruptedException e1) {
                            throw new RuntimeException(e1);
                        }

                        tryCount++;
                    }
                }
                // otherwise just pass the original response on
                return response;
            }
        });

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(new TypeToken<RealmList<RealmString>>() {}.getType(),
                        new TagRealmListConverter())
                .create();
        if (REST_CLIENT == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client.build())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            REST_CLIENT = retrofit.create(APIService.class);
        }
    }
}
