package in.limra.busdriver.network;

import java.util.HashMap;

import in.limra.busdriver.response.loginresponse.Response;
import in.limra.busdriver.response.loginresponse.batchresponse.BatchListResponse;
import in.limra.busdriver.response.loginresponse.busresponse.BusResponse;
import in.limra.busdriver.response.loginresponse.driverresponse.DriverResponse;
import in.limra.busdriver.response.loginresponse.helpresponse.HelpResponse;
import in.limra.busdriver.response.loginresponse.pointresponse.PointResponse;
import in.limra.busdriver.response.loginresponse.reasonresponse.NotificationReasonList;
import in.limra.busdriver.response.loginresponse.reasonresponse.ReasonResponse;
import in.limra.busdriver.response.loginresponse.routelistresponse.RouteResponse;
import in.limra.busdriver.response.loginresponse.studentlistresponse.StudentListResponse;
import in.limra.busdriver.response.loginresponse.studentlistresponse.StudentListResponseFromPickUp;
import in.limra.busdriver.response.loginresponse.unassignresponse.UnassignResponse;
import in.limra.busdriver.response.loginresponse.unassignresponse.UnassignedStudentList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIService {

    @POST("doLogin")
    Call<Response> login(@Body HashMap<String,String> hashMap);

    @POST("logout")
    Call<Response> logout(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("getPickupPointList")
    Call<PointResponse> getPickupPointList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("createPickupPoint")
    Call<Response> createPickupPoint(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @GET("getBusList")
    Call<BusResponse> getBusList(@Header("Authorization") String auth);

    @GET("getRouteList")
    Call<RouteResponse> getRouteList(@Header("Authorization") String auth);

    @POST("getBatchList")
    Call<BatchListResponse> getBatchList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("StartTrip")
    Call<Response> StartTrip(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("getStudentList")
    Call<StudentListResponse> getStudentList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("getStudentFromPickUpPoint")
    Call<StudentListResponseFromPickUp> getStudentListFromPickUp(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("unAssignedStudent")
    Call<Response> unAssignedStudent(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("studentAttendance")
    Call<Response> updateStudentAttendance(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("unAssignedStudentList")
    Call<UnassignResponse> unAssignedStudentList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("assignStudentToPoint")
    Call<Response> AssignedStudent(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("EndTrip")
    Call<Response> endTrip(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @GET("getNotificationReasonList")
    Call<ReasonResponse> getNotificationReasonList(@Header("Authorization") String auth);

    @GET("getDriverDetails")
    Call<DriverResponse> getDriverDetails(@Header("Authorization") String auth);


    @GET("getHelpConatctDetailsList")
    Call<HelpResponse> getHelpConatctDetailsList(@Header("Authorization") String auth);

    @POST("getRoutePointsList")
    Call<in.limra.busdriver.response.loginresponse.maprouteresponse.RouteResponse> getRoutePointsList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("changeBusTracking")
    Call<Response> changeBusTracking(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("checkBusTracking")
    Call<Response> checkBusTracking(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("reachAtPoint")
    Call<Response> reachAtPoint(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);
}
