package in.limra.busdriver.service;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import in.limra.busdriver.R;
import in.limra.busdriver.SingleShotLocationProvider;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.SharedPreferenceHelper;


public class GPSTracker extends Service implements LocationListener {

    Context mContext;

    // Flag for GPS status
    boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    // Flag for GPS status
    boolean canGetLocation = false;

    Location location; // Location
    int locationMode =0;


    FirebaseDatabase userDB;
    DatabaseReference myRef;

    private SingleShotLocationProvider singleShotLocationProvider;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES =0; // 10 sec

    // Declaring a Location Manager
    protected LocationManager locationManager;

    Task<LocationSettingsResponse> task;

    public GPSTracker(Context context) {
        this.mContext = context;
        userDB = FirebaseDatabase.getInstance();
        myRef = userDB.getReference(mContext.getResources().getString(R.string.app_name));

        getLocation();
    }


    @SuppressLint("MissingPermission")
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // Getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // Getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // No network provider is enabled
            } else {
                this.canGetLocation = true;
                // If GPS enabled, get latitude/longitude using GPS Services

                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {



                                    singleShotLocationProvider = new SingleShotLocationProvider(mContext);
                                    if(SharedPreferenceHelper.getInstance(mContext).isTracking()) {

                                        Location loc=singleShotLocationProvider.getLoc();
                                        if(loc!=null) {

                                           // String address = AppUtils.getCompleteAddressString(mContext, loc.getLatitude(), loc.getLongitude());

                                            int speed = (int) ((loc.getSpeed() * 3600) / 1000);
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID));
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(loc.getLatitude()));
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(loc.getLongitude()));
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                                            //myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("address").setValue(address);



                                        }else{
                                           // String address = AppUtils.getCompleteAddressString(mContext, location.getLatitude(), location.getLongitude());
                                            int speed = (int) ((location.getSpeed() * 3600) / 1000);
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID));
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(location.getLatitude()));
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(location.getLongitude()));
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                                          //  myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("address").setValue(address);

                                        }


                                    }else{
                                        if(myRef!=null && SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)!=null)
                                            myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).removeValue();
                                    }


                            }
                        }
                    }
                }

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {


                                singleShotLocationProvider = new SingleShotLocationProvider(mContext);
                                if(SharedPreferenceHelper.getInstance(mContext).isTracking()) {

                                    Location loc=singleShotLocationProvider.getLoc();
                                    if(loc!=null) {

                                     // String address = AppUtils.getCompleteAddressString(mContext, loc.getLatitude(), loc.getLongitude());

                                        int speed = (int) ((loc.getSpeed() * 3600) / 1000);
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID));
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(loc.getLatitude()));
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(loc.getLongitude()));
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                                      //  myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("address").setValue(address);



                                    }else{
                                      //  String address = AppUtils.getCompleteAddressString(mContext, location.getLatitude(), location.getLongitude());
                                        int speed = (int) ((location.getSpeed() * 3600) / 1000);
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID));
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(location.getLatitude()));
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(location.getLongitude()));
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                                     //   myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("address").setValue(address);



                                    }


                                }else{
                                    if(myRef!=null && SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)!=null)
                                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).removeValue();
                                }


                        }
                    }
                }



            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog.
     * On pressing the Settings button it will launch Settings Options.
     * */


    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });
        // On pressing the cancel button

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public void onLocationChanged(Location location) {
       // getLocation();
        if (location != null) {


                singleShotLocationProvider = new SingleShotLocationProvider(mContext);
                if(SharedPreferenceHelper.getInstance(mContext).isTracking()) {
                    Toast.makeText(mContext, "Location From GPS Changed from Service", Toast.LENGTH_SHORT).show();

                    Location loc=singleShotLocationProvider.getLoc();
                    if(loc!=null) {

                      //  String address = AppUtils.getCompleteAddressString(mContext, loc.getLatitude(), loc.getLongitude());

                        int speed = (int) ((loc.getSpeed() * 3600) / 1000);
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID));
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(loc.getLatitude()));
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(loc.getLongitude()));
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                      //  myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("address").setValue(address);

                    }else{
                       // String address = AppUtils.getCompleteAddressString(mContext, location.getLatitude(), location.getLongitude());
                        int speed = (int) ((location.getSpeed() * 3600) / 1000);
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID));
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(location.getLatitude()));
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(location.getLongitude()));
                        myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                      //  myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).child("address").setValue(address);

                    }


                }else{
                    if(myRef!=null && SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)!=null)
                    myRef.child(SharedPreferenceHelper.getInstance(mContext).getString(Constants.TRIP_ID)).removeValue();

            }
        }
    }


    @Override
    public void onProviderDisabled(String provider) {
    }


    @Override
    public void onProviderEnabled(String provider) {
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
