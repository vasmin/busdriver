package in.limra.busdriver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import customfont.MyTextView;
import in.limra.busdriver.R;
import in.limra.busdriver.response.loginresponse.pointresponse.PickupPoint;

public class TripAdapter extends RecyclerView.Adapter {


    private final LayoutInflater layoutInflater;
    private final Context context;
    private List<PickupPoint> list=new ArrayList<>();
    OnItemClickListner onItemClickListner;

    public TripAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<PickupPoint> list){
        this.list=list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListner {
        void onItemClick(PickupPoint d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_trip, parent, false);
        return new TripAdapter.TripHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final PickupPoint pickupPoint=list.get(position);
        if(holder instanceof  TripHolder){
            ((TripHolder) holder).tripLocation.setText(pickupPoint.getStop_name());
            ((TripHolder) holder).sequenceNoText.setText(pickupPoint.getSequenceNo());

            if (pickupPoint.isFlag())
            {
                ((TripHolder) holder).cardView.setCardBackgroundColor(context.getResources().getColor(R.color.card_color));
            }




            ((TripHolder) holder).bind(pickupPoint,onItemClickListner);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class TripHolder extends RecyclerView.ViewHolder {
        TextView tripLocation,sequenceNoText;
        CardView cardView;
        public TripHolder(View v) {
            super(v);
            tripLocation =v.findViewById(R.id.triplocation);
            sequenceNoText = v.findViewById(R.id.sequenceNoText);
            cardView = v.findViewById(R.id.card);
        }

        public void bind(final PickupPoint data, OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }

    }
}
