package in.limra.busdriver.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import in.limra.busdriver.R;
import in.limra.busdriver.data.SpinnerModel;

public class SpinnerAdapter extends ArrayAdapter {
    private Activity activity;
    private List<SpinnerModel> alSpinner=new ArrayList<>();

    public SpinnerAdapter(@NonNull Activity activity) {
        super(activity,0);
        this.activity = activity;

    }

    public  void setData(List<SpinnerModel> alSpinner){
        this.alSpinner = alSpinner;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);
        ((TextView)view.findViewById(R.id.tvName)).setText(alSpinner.get(position).getName());
        return view;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);

        ((TextView) view.findViewById(R.id.tvName)).setText(alSpinner.get(position).getName());
        if(position == 0){
            // Set the hint text color gray
            ((TextView) view.findViewById(R.id.tvName)).setTextColor(Color.GRAY);
        }
        return view;
    }

    @Override
    public int getCount() {
        return alSpinner.size();
    }
}
