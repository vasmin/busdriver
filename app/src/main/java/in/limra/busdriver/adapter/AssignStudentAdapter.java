package in.limra.busdriver.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import customfont.MyTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import in.limra.busdriver.R;
import in.limra.busdriver.response.loginresponse.pointresponse.PickupPoint;
import in.limra.busdriver.response.loginresponse.studentlistresponse.Student_List_From_PickUp;

public class AssignStudentAdapter extends RecyclerView.Adapter {

    List<Student_List_From_PickUp> list=new ArrayList<>();

    private final LayoutInflater layoutInflater;
    private final Context context;
    OnItemClickListner onItemClickListner;

    public AssignStudentAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<Student_List_From_PickUp> list){
        this.list=list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListner {
        void onItemClick(Student_List_From_PickUp d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_assign, parent, false);
        return new AssignStudentAdapter.AsignHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Student_List_From_PickUp student_list_from_pickUp=list.get(position);

        if(holder instanceof AsignHolder){
            ((AsignHolder) holder).stdName.setText(student_list_from_pickUp.getStudentName());
            ((AsignHolder) holder).fatherName.setText(student_list_from_pickUp.getFatherName());
            ((AsignHolder) holder).motherName.setText(student_list_from_pickUp.getMotherName());
            ((AsignHolder) holder).fatherMobile.setText(student_list_from_pickUp.getFatherMobileNumber());
            ((AsignHolder) holder).motherMobile.setText(student_list_from_pickUp.getMotherMobileNumber());
            ((AsignHolder) holder).stdBatch.setText(student_list_from_pickUp.getBatchShift());

            ((AsignHolder) holder).type.setText(student_list_from_pickUp.getType());





            Glide.with(context)
                    .load(student_list_from_pickUp.getImgUrl())
                    .apply(RequestOptions.placeholderOf(R.drawable.student2).error(R.drawable.student2))
                    .into(((AsignHolder) holder).stdImage);

            ((AsignHolder) holder).arrowUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((AsignHolder) holder).arrowUp.setVisibility(View.GONE);
                    ((AsignHolder) holder).arrowImg.setVisibility(View.VISIBLE);
                    ((AsignHolder) holder).linearLayout.setVisibility(View.GONE);

                }
            });

            ((AsignHolder) holder).arrowImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((AsignHolder) holder).arrowUp.setVisibility(View.VISIBLE);
                    ((AsignHolder) holder).arrowImg.setVisibility(View.GONE);
                    ((AsignHolder) holder).linearLayout.setVisibility(View.VISIBLE);

                }
            });

            ((AsignHolder) holder).motherCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Permission is not granted
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + ((AsignHolder) holder).motherMobile.getText().toString()));
                        context.startActivity(callIntent);
                    }


                }
            });

            if(student_list_from_pickUp.getType().equals("pick")){
                if(student_list_from_pickUp.getStatus().equals("Home")){

                    ((AsignHolder) holder).pickLinear.setVisibility(View.VISIBLE);
                    ((AsignHolder) holder).dropLinear.setVisibility(View.GONE);

                }else if(student_list_from_pickUp.getStatus().equals("In Bus")){
                    ((AsignHolder) holder).pickLinear.setVisibility(View.GONE);
                    ((AsignHolder) holder).dropLinear.setVisibility(View.VISIBLE);
                }else{
                    ((AsignHolder) holder).dropBtn.setText("Dropped");
                    ((AsignHolder) holder).pickLinear.setVisibility(View.GONE);
                    ((AsignHolder) holder).dropLinear.setVisibility(View.VISIBLE);
                    ((AsignHolder) holder).dropLinear.setClickable(false);
                    ((AsignHolder) holder).dropLinear.setEnabled(false);
                }
            }else{
                if(student_list_from_pickUp.getStatus().equals("School")){

                    ((AsignHolder) holder).pickLinear.setVisibility(View.VISIBLE);
                    ((AsignHolder) holder).dropLinear.setVisibility(View.GONE);

                }else if(student_list_from_pickUp.getStatus().equals("In Bus")){
                    ((AsignHolder) holder).pickLinear.setVisibility(View.GONE);
                    ((AsignHolder) holder).dropLinear.setVisibility(View.VISIBLE);
                }else{
                    ((AsignHolder) holder).dropBtn.setText("Dropped");
                    ((AsignHolder) holder).pickLinear.setVisibility(View.GONE);
                    ((AsignHolder) holder).dropLinear.setVisibility(View.VISIBLE);
                    ((AsignHolder) holder).dropLinear.setClickable(false);
                    ((AsignHolder) holder).dropLinear.setEnabled(false);
                }
            }


            ((AsignHolder) holder).bind(student_list_from_pickUp,onItemClickListner);

            ((AsignHolder) holder).fatherCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Permission is not granted
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + ((AsignHolder) holder).fatherMobile.getText().toString()));
                        context.startActivity(callIntent);
                    }

                }
            });


        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AsignHolder extends RecyclerView.ViewHolder {
        ImageView toggleImg, arrowImg, arrowUp, fatherCall, motherCall;
        MyTextView stdName, stdBatch, stdStatus, fatherName, motherName, fatherMobile, motherMobile;
        LinearLayout linearLayout, unassignLinear, assignLinear,pickLinear,dropLinear;

        @BindView(R.id.pickbtn)
        TextView pickBtn;

        @BindView(R.id.dropbtn)
        TextView dropBtn;

        @BindView(R.id.type)
                TextView type;

        CircleImageView stdImage;
        public AsignHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
            stdImage = (CircleImageView) itemView.findViewById(R.id.stdImg);
            arrowImg = (ImageView) itemView.findViewById(R.id.arrowDown);
            arrowUp = (ImageView) itemView.findViewById(R.id.arrowUp);
            toggleImg = (ImageView) itemView.findViewById(R.id.toggleImg);
            fatherCall = (ImageView) itemView.findViewById(R.id.fathercall);
            motherCall = (ImageView) itemView.findViewById(R.id.mcall);

            stdName = (MyTextView) itemView.findViewById(R.id.stdName);
            stdBatch = (MyTextView) itemView.findViewById(R.id.stdBatch);
            stdStatus = (MyTextView) itemView.findViewById(R.id.stdStatus);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear);

            fatherName = (MyTextView) itemView.findViewById(R.id.fatherName);
            motherName = (MyTextView) itemView.findViewById(R.id.mothername);
            fatherMobile = (MyTextView) itemView.findViewById(R.id.fathermobile);
            motherMobile = (MyTextView) itemView.findViewById(R.id.mmobile);

            assignLinear = (LinearLayout) itemView.findViewById(R.id.assign);
            unassignLinear = (LinearLayout) itemView.findViewById(R.id.unassign);

            pickLinear=v.findViewById(R.id.pick);
            dropLinear=v.findViewById(R.id.drop);

        }


        public void bind(final Student_List_From_PickUp data, OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
