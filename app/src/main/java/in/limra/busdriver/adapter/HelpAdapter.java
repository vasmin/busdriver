package in.limra.busdriver.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.response.loginresponse.helpresponse.HelpContactList;
import in.limra.busdriver.response.loginresponse.pointresponse.PickupPoint;

public class HelpAdapter extends RecyclerView.Adapter {
    List<HelpContactList> list=new ArrayList<>();
    LayoutInflater layoutInflater;
    Context context;
    OnItemClickListner onItemClickListner;


    public HelpAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<HelpContactList> list){
        this.list=list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListner {
        void onItemClick(PickupPoint d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_help, parent, false);
        return new HelpAdapter.HelpHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        HelpContactList helpContactList=list.get(position);

        if(holder instanceof HelpHolder){
            ((HelpHolder) holder).mobile.setText(helpContactList.getContactNumber());
            ((HelpHolder) holder).name.setText(helpContactList.getName());

            ((HelpHolder) holder).callLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Permission is not granted
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + helpContactList.getContactNumber()));
                        context.startActivity(callIntent);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HelpHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.mobile)
        TextView mobile;

        @BindView(R.id.call)
        LinearLayout callLinear;


        public HelpHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }
}
