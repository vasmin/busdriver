package in.limra.busdriver.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import customfont.MyTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import in.limra.busdriver.R;
import in.limra.busdriver.response.loginresponse.studentlistresponse.Student_List_From_PickUp;
import in.limra.busdriver.response.loginresponse.unassignresponse.UnassignedStudentList;

public class UnassignStudentAdapter extends RecyclerView.Adapter {

    List<UnassignedStudentList> list=new ArrayList<>();

    private final LayoutInflater layoutInflater;
    private final Context context;
    OnItemClickListner onItemClickListner;

    public UnassignStudentAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<UnassignedStudentList> list){
        this.list=list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListner {
        void onItemClick(UnassignedStudentList d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_unassign, parent, false);
        return new UnassignStudentAdapter.UnAsignHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        UnassignedStudentList unassignedStudentList=list.get(position);

        if(holder instanceof UnAsignHolder){
            ((UnAsignHolder) holder).stdName.setText(unassignedStudentList.getStudentName());
            ((UnAsignHolder) holder).stdBatch.setText(unassignedStudentList.getBatch());

            ((UnAsignHolder) holder).type.setText(unassignedStudentList.getType());

            Glide.with(context)
                    .load(unassignedStudentList.getPhoto())
                    .into(((UnAsignHolder) holder).stdImage);

            ((UnAsignHolder) holder).bind(unassignedStudentList,onItemClickListner);



        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class UnAsignHolder extends RecyclerView.ViewHolder {
        MyTextView stdName, stdBatch,type;

        CircleImageView stdImage;
        public UnAsignHolder(View v) {
            super(v);
            stdImage = (CircleImageView) itemView.findViewById(R.id.stdImg);
            stdName = (MyTextView) itemView.findViewById(R.id.stdName);
            stdBatch = (MyTextView) itemView.findViewById(R.id.stdBatch);
            type=v.findViewById(R.id.type);


        }


        public void bind(final UnassignedStudentList data, OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
