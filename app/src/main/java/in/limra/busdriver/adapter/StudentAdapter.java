package in.limra.busdriver.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.data.RealmHelper;
import in.limra.busdriver.response.loginresponse.studentlistresponse.StudentList;
import io.realm.Realm;

public class StudentAdapter extends RecyclerView.Adapter {
    private final LayoutInflater layoutInflater;
    private final Context context;
    private List<StudentList> list=new ArrayList<>();
    private final OnItemClickListener onItemClickListener;

    private Realm realm;

    public StudentAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(StudentList d, View view) throws ParseException;
    }

    public void setData(List<StudentList> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_student, viewGroup, false);
        return new StudentAdapter.StudentHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int i) {
        final StudentList studentList=list.get(i);
        if(holder instanceof StudentAdapter.StudentHolder){
            ((StudentHolder) holder).name.setText(studentList.getStudentName());
            ((StudentHolder) holder).address.setText(studentList.getAddress());

            realm= RealmHelper.getRealmInstance();

            if(!studentList.isView()){
                ((StudentHolder) holder).contactImage.setImageDrawable(context.getDrawable(R.drawable.arrow));
                ((StudentHolder) holder).linearLayout.setVisibility(View.GONE);
            }else{
                ((StudentHolder) holder).contactImage.setImageDrawable(context.getDrawable(R.drawable.up_arrow));
                ((StudentHolder) holder).linearLayout.setVisibility(View.VISIBLE);
            }

            ((StudentHolder) holder).fatherName.setText(studentList.getFatherName());
            ((StudentHolder) holder).mobile.setText(studentList.getFatherMobileNumber());

            ((StudentHolder) holder).motherName.setText(studentList.getMotherName());
            ((StudentHolder) holder).motherMobile.setText(studentList.getMotherMobileNumber());

            if(studentList.getType().equals("pick")){
                ((StudentHolder) holder).dropLinear.setVisibility(View.GONE);
                ((StudentHolder) holder).pickLinear.setVisibility(View.VISIBLE);

                if(studentList.getStatus().equals("In Bus")){
                    ((StudentHolder) holder).pickinbusView.setBackgroundColor(Color.GREEN);
                }else if(studentList.getStatus().equals("School")){
                    ((StudentHolder) holder).pickoutbusView.setBackgroundColor(Color.GREEN);
                    ((StudentHolder) holder).pickinbusView.setBackgroundColor(Color.GREEN);
                }else {
                    ((StudentHolder) holder).pickoutbusView.setBackgroundColor(Color.GRAY);
                    ((StudentHolder) holder).pickinbusView.setBackgroundColor(Color.GRAY);

                }

            }else{
                ((StudentHolder) holder).dropLinear.setVisibility(View.VISIBLE);
                ((StudentHolder) holder).pickLinear.setVisibility(View.GONE);

                if(studentList.getStatus().equals("In Bus")){
                    ((StudentHolder) holder).dropinhomeView.setBackgroundColor(Color.GREEN);
                }else if(studentList.getStatus().equals("Home")){
                    ((StudentHolder) holder).dropinbusView.setBackgroundColor(Color.GREEN);
                    ((StudentHolder) holder).dropinhomeView.setBackgroundColor(Color.GREEN);
                }else {
                    ((StudentHolder) holder).dropinbusView.setBackgroundColor(Color.GRAY);
                    ((StudentHolder) holder).dropinhomeView.setBackgroundColor(Color.GRAY);

                }
            }

            ((StudentHolder) holder).batch.setText(studentList.getBatch());

            ((StudentHolder) holder).bind(studentList, onItemClickListener);

        }
        ((StudentHolder) holder).motherCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Permission is not granted
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + studentList.getMotherMobileNumber()));
                    context.startActivity(callIntent);
                }
            }
        });

        Glide.with(context)
                .load(studentList.getPhoto())
                .into(((StudentHolder) holder).studentImg);

        ((StudentHolder) holder).fatherCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Permission is not granted
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + studentList.getFatherMobileNumber()));
                    context.startActivity(callIntent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class StudentHolder extends RecyclerView.ViewHolder {
        final TextView name;
        final TextView address;
        final ImageView contactImage;
        final LinearLayout linearLayout;

        @BindView(R.id.fathername)
        TextView fatherName;

        @BindView(R.id.mobile)
        TextView mobile;

        @BindView(R.id.fathercall)
        ImageView fatherCall;

        @BindView(R.id.mothername)
        TextView motherName;

        @BindView(R.id.mmobile)
        TextView motherMobile;

        @BindView(R.id.mcall)
        ImageView motherCall;

        @BindView(R.id.pickinbus)
        View pickinbusView;

        @BindView(R.id.pickoutbus)
        View pickoutbusView;

        @BindView(R.id.dropoutbus)
        View dropinbusView;

        @BindView(R.id.dropouthome)
        View dropinhomeView;

        @BindView(R.id.droplinear)
        LinearLayout dropLinear;

        @BindView(R.id.picklinear)
        LinearLayout pickLinear;

        @BindView(R.id.batch)
        TextView batch;

        @BindView(R.id.student)
                ImageView studentImg;

        StudentHolder(View v) {
            super(v);
            name=v.findViewById(R.id.name);
            address=v.findViewById(R.id.address);
            contactImage=v.findViewById(R.id.contact);
            linearLayout=v.findViewById(R.id.linear);
            ButterKnife.bind(this,v);
        }
        void bind(final StudentList data, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listener.onItemClick(data, v);
                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }

    }
}
