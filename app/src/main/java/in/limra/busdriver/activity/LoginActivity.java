package in.limra.busdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.Response;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.password)
    EditText password;

    ProgressDialog progressBar;

    Location location;
    String latitude="";
    String longitude="";

    boolean isShow=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setBackgroundDrawableResource(R.drawable.login_bg);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        AppUtils.checkAndRequestPermissions(this);


        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(mobile.getText().toString())){
                    mobile.setError("Enter mobile ");
                    mobile.requestFocus();
                    return;
                }

                if(!AppUtils.isValidMobile(mobile.getText().toString())){
                    mobile.setError("Enter valid mobile");
                    mobile.requestFocus();
                    return;
                }

                if(TextUtils.isEmpty(password.getText().toString())){
                    password.setError("Enter password ");
                    password.requestFocus();
                    return;
                }



                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                } catch(Exception ignored) {
                    ignored.printStackTrace();
                }

                loginApi();
            }
        });
    }


    public void loginApi(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("mobile",mobile.getText().toString());
        hashMap.put("password",password.getText().toString());
        Call<Response> call= RestClient.get().login(hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()) {

                        AppUtils.snackBar(LoginActivity.this,response1.getMessage());

                        SharedPreferenceHelper.getInstance(getApplicationContext()).saveAuthToken(response1.getToken());
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(true);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).isFirstLogin(true);
                        if(response1.isTrip()){
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BATCH_ID,response1.getBatch_id());
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.TRIP_ID,response1.getTrip_id());
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BUS_ID,response1.getBus_id());
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.ROUTE_ID,response1.getRoute_id());
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setTripStart(true);
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(true);
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.ACCOUNT_TYPE,response1.getType());

                            if(response1.getType().equals("Conductor")) {
                                Intent intent = new Intent(LoginActivity.this, ConductorHomeActivity.class);
                                startActivity(intent);
                                finish();
                            }else{
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        }else{
                            Intent intent = new Intent(LoginActivity.this, StartTripActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.ACCOUNT_TYPE,response1.getType());

                    }else{
                        AppUtils.errorSnackBar(LoginActivity.this,response1.getMessage());
                    }
                }else{
                    AppUtils.errorSnackBar(LoginActivity.this,Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                AppUtils.errorSnackBar(LoginActivity.this,Constants.SERVERTIMEOUT);
            }
        });
    }
}
