package in.limra.busdriver.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.adapter.SpinnerAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.RealmHelper;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.data.SpinnerModel;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.batchresponse.BatchDetail;
import in.limra.busdriver.response.loginresponse.batchresponse.BatchListResponse;
import in.limra.busdriver.response.loginresponse.busresponse.BusList;
import in.limra.busdriver.response.loginresponse.busresponse.BusResponse;
import in.limra.busdriver.response.loginresponse.routelistresponse.RouteList;
import in.limra.busdriver.response.loginresponse.routelistresponse.RouteResponse;
import in.limra.busdriver.service.GPSTracker;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartTripActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    SpinnerAdapter busNumberAdapter,routeAdapter,routeTypeAdapter,pickBatchAdapter,dropBatchAdapter;

    @BindView(R.id.busnumber)
    Spinner busSpinner;

    @BindView(R.id.route)
    Spinner routeSpinner;

    @BindView(R.id.routetype)
    Spinner routeTypeSpinner;

    @BindView(R.id.dropspinner)
    Spinner dropSpinner;

    @BindView(R.id.pickspinner)
    Spinner pickSpinner;

    @BindView(R.id.dropcard)
    CardView dropCard;

    @BindView(R.id.pickcard)
    CardView pickCard;

    @BindView(R.id.starttrip)
    CardView startTrip;


    List<SpinnerModel> list=new ArrayList<>();
    List<SpinnerModel> buslist=new ArrayList<>();
    List<SpinnerModel> routeList=new ArrayList<>();
    List<SpinnerModel> pickbatchList=new ArrayList<>();
    List<SpinnerModel> dropbatchList=new ArrayList<>();

    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    Location location;

    String busId="0";
    String routeId="0";
    String routeTypeId="0";
    String pickbatchId="0";
    String dropbatchId="0";

    Realm realm;
    GPSTracker gps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_trip);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        realm= RealmHelper.getRealmInstance();

        busNumberAdapter=new SpinnerAdapter(this);
        routeAdapter=new SpinnerAdapter(this);
        routeTypeAdapter=new SpinnerAdapter(this);
        pickBatchAdapter=new SpinnerAdapter(this);
        dropBatchAdapter=new SpinnerAdapter(this);


        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setUpGClient();


        getWindow().setBackgroundDrawableResource(R.drawable.dashboard_bg);

        list.clear();

        SpinnerModel selectModel=new SpinnerModel();
        selectModel.setId("0");
        selectModel.setName("Select Route Type");
        list.add(selectModel);

        SpinnerModel spinnerModel=new SpinnerModel();
        spinnerModel.setId("1");
        spinnerModel.setName("Pick");
        list.add(spinnerModel);

        SpinnerModel spinnerModel1=new SpinnerModel();
        spinnerModel1.setId("2");
        spinnerModel1.setName("Drop");
        list.add(spinnerModel1);

        SpinnerModel spinnerModel2=new SpinnerModel();
        spinnerModel2.setId("3");
        spinnerModel2.setName("Both");
        list.add(spinnerModel2);

        routeTypeAdapter.setData(list);
        routeTypeSpinner.setAdapter(routeTypeAdapter);

        buslist.clear();
        SpinnerModel bus=new SpinnerModel();
        bus.setId("0");
        bus.setName("Select Bus Number");
        buslist.add(bus);

        busNumberAdapter.setData(buslist);
        busSpinner.setAdapter(busNumberAdapter);

        routeList.clear();
        SpinnerModel route=new SpinnerModel();
        route.setId("0");
        route.setName("Select Route");
        routeList.add(route);

        routeAdapter.setData(routeList);
        routeSpinner.setAdapter(routeAdapter);


        pickbatchList.clear();
        SpinnerModel pick=new SpinnerModel();
        pick.setId("0");
        pick.setName("Select Pick Batch");
        pickbatchList.add(pick);

        pickBatchAdapter.setData(pickbatchList);
        pickSpinner.setAdapter(pickBatchAdapter);

        dropbatchList.clear();
        SpinnerModel drop=new SpinnerModel();
        drop.setId("0");
        drop.setName("Select Drop Batch");
        dropbatchList.add(drop);

        dropBatchAdapter.setData(dropbatchList);
        dropSpinner.setAdapter(dropBatchAdapter);


        getBusNumber();
        getRouteList();
        getBatchList();

        busSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(buslist.size()>position) {
                    busId = buslist.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        routeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(routeList.size()>position) {
                    routeId = routeList.get(position).getId();
                    getBatchList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        routeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(list.size()>position) {
                    routeTypeId = list.get(position).getId();

                    if(routeTypeId.equals("0")){
                        pickCard.setVisibility(View.VISIBLE);
                        dropCard.setVisibility(View.GONE);
                    }else if(routeTypeId.equals("1")){
                        pickCard.setVisibility(View.VISIBLE);
                        dropCard.setVisibility(View.GONE);
                    }else if(routeTypeId.equals("2")){
                        pickCard.setVisibility(View.GONE);
                        dropCard.setVisibility(View.VISIBLE);
                    }else{
                        pickCard.setVisibility(View.VISIBLE);
                        dropCard.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        pickSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(pickbatchList.size()>position) {
                    pickbatchId = pickbatchList.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        dropSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(dropbatchList.size()>position) {
                    dropbatchId = dropbatchList.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        startTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(busId.equals("0")){

                    AppUtils.errorSnackBar(StartTripActivity.this,"Select Bus Number");
                    return;
                }


                if(routeId.equals("0")){
                    AppUtils.errorSnackBar(StartTripActivity.this,"Select Route");
                    return;

                }

                if(routeTypeId.equals("0")){
                    AppUtils.errorSnackBar(StartTripActivity.this,"Select Route Type");
                    return;
                }else if(routeTypeId.equals("1")){
                    if(pickbatchId.equals("0")){
                        AppUtils.errorSnackBar(StartTripActivity.this,"Select Pick Batch");
                        return;
                    }
                }else if(routeTypeId.equals("2")){

                    if(dropbatchId.equals("0")){
                        AppUtils.errorSnackBar(StartTripActivity.this,"Select Drop Batch");
                        return;
                    }
                }else if(routeTypeId.equals("3")){

                    if(pickbatchId.equals("0")){
                        AppUtils.errorSnackBar(StartTripActivity.this,"Select Pick Batch");
                        return;
                    }

                    if(dropbatchId.equals("0")){
                        AppUtils.errorSnackBar(StartTripActivity.this,"Select Drop Batch");
                        return;
                    }
                    if(pickbatchId.equals(dropbatchId)){
                        AppUtils.errorSnackBar(StartTripActivity.this,"Pick and Drop batch should not be same");
                        return;
                    }
                }

                StartTrip();


            }
        });
    }

    public void getBusNumber(){

        Call<BusResponse> call= RestClient.get().getBusList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<BusResponse>() {
            @Override
            public void onResponse(Call<BusResponse> call, Response<BusResponse> response) {
                BusResponse busResponse=response.body();
                if(response.code()==200){
                    if(busResponse.getStatus()){

                        for(int i=0;i<busResponse.getBusList().size();i++){
                            BusList busList=busResponse.getBusList().get(i);
                            SpinnerModel spinnerModel1=new SpinnerModel();
                            spinnerModel1.setId(busList.getBusId());
                            spinnerModel1.setName(busList.getBusNumber());

                            if(!buslist.contains(spinnerModel1)) {
                                buslist.add(spinnerModel1);
                            }
                        }
                        busNumberAdapter.setData(buslist);

                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(StartTripActivity.this);
                }else{

                }

            }

            @Override
            public void onFailure(Call<BusResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getRouteList(){

        Call<RouteResponse> call= RestClient.get().getRouteList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<RouteResponse>() {
            @Override
            public void onResponse(Call<RouteResponse> call, Response<RouteResponse> response) {
                RouteResponse busResponse=response.body();
                if(response.code()==200){
                    if(busResponse.getStatus()){

                        for(int i=0;i<busResponse.getRouteList().size();i++){
                            RouteList routelist=busResponse.getRouteList().get(i);
                            SpinnerModel spinnerModel1=new SpinnerModel();
                            spinnerModel1.setId(routelist.getRouteId());
                            spinnerModel1.setName(routelist.getRouteName());

                            if(!routeList.contains(spinnerModel1)) {
                                routeList.add(spinnerModel1);
                            }
                        }
                        routeAdapter.setData(routeList);

                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(StartTripActivity.this);
                }else{

                }

            }

            @Override
            public void onFailure(Call<RouteResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getBatchList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("route_id",routeId);
        Call<BatchListResponse> call= RestClient.get().getBatchList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<BatchListResponse>() {
            @Override
            public void onResponse(Call<BatchListResponse> call, Response<BatchListResponse> response) {
                BatchListResponse batchListResponse=response.body();
                if(response.code()==200){
                    if(batchListResponse.getStatus()){

                        pickbatchList.clear();
                        SpinnerModel pick=new SpinnerModel();
                        pick.setId("0");
                        pick.setName("Select Pick Batch");
                        pickbatchList.add(pick);

                        dropbatchList.clear();
                        SpinnerModel drop=new SpinnerModel();
                        drop.setId("0");
                        drop.setName("Select Drop Batch");
                        dropbatchList.add(drop);

                        for(int i=0;i<batchListResponse.getBatchDetails().size();i++){
                            BatchDetail batchDetail=batchListResponse.getBatchDetails().get(i);
                            SpinnerModel spinnerModel1=new SpinnerModel();
                            spinnerModel1.setId(batchDetail.getBatchId());
                            spinnerModel1.setName(batchDetail.getBatchName());

                            if(!pickbatchList.contains(spinnerModel1)) {
                                pickbatchList.add(spinnerModel1);
                            }
                            if(!dropbatchList.contains(spinnerModel1)) {
                                dropbatchList.add(spinnerModel1);
                            }
                        }
                        dropBatchAdapter.setData(dropbatchList);
                        pickBatchAdapter.setData(pickbatchList);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(StartTripActivity.this);
                }
            }

            @Override
            public void onFailure(Call<BatchListResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        MenuItem logout = menu.findItem(R.id.logout);
        logout.setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id==R.id.logout){
            logout();
        }



        return super.onOptionsItemSelected(item);
    }

    public void logout() {

        HashMap<String, String> hashMap = new HashMap<>();
        Call<in.limra.busdriver.response.loginresponse.Response> call = RestClient.get().logout(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(), hashMap);
        call.enqueue(new Callback<in.limra.busdriver.response.loginresponse.Response>() {
            @Override
            public void onResponse(Call<in.limra.busdriver.response.loginresponse.Response> call, retrofit2.Response<in.limra.busdriver.response.loginresponse.Response> response) {
                in.limra.busdriver.response.loginresponse.Response response1 = response.body();
                if (response.code() == 200) {
                    if (response1.getStatus()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.deleteAll();
                            }
                        });
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(false);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.ACCOUNT_TYPE,"");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.TRIP_ID,"");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BATCH_ID,"");
                        Intent intent = new Intent(StartTripActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        AppUtils.errorSnackBar(StartTripActivity.this, response1.getMessage());
                    }
                } else {
                    AppUtils.errorSnackBar(StartTripActivity.this, Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<in.limra.busdriver.response.loginresponse.Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(StartTripActivity.this, Constants.SERVERTIMEOUT);
            }
        });
    }

    public void StartTrip(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("route_id",routeId);
        hashMap.put("bus_id",busId);

        if(routeTypeId.equals("1")){
            hashMap.put("batch_id","pick-"+pickbatchId);
        }else if(routeTypeId.equals("2")){
            hashMap.put("batch_id","drop-"+dropbatchId);
        }else if(routeTypeId.equals("3")){
            hashMap.put("batch_id","pick-"+pickbatchId+","+"drop-"+dropbatchId);
        }

        Call<in.limra.busdriver.response.loginresponse.Response> call= RestClient.get().StartTrip(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.limra.busdriver.response.loginresponse.Response>() {
            @Override
            public void onResponse(Call<in.limra.busdriver.response.loginresponse.Response> call, Response<in.limra.busdriver.response.loginresponse.Response> response) {
                in.limra.busdriver.response.loginresponse.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.ROUTE_ID, routeId);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BUS_ID, busId);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setTripStart(true);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.TRIP_ID,response1.getTrip_id());

                        if(routeTypeId.equals("1")){
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BATCH_ID,pickbatchId);

                        }else if(routeTypeId.equals("2")){
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BATCH_ID,dropbatchId);
                        }else{
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BATCH_ID,"");
                        }

                        if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.ACCOUNT_TYPE).equals("Conductor")) {
                            Intent intent = new Intent(StartTripActivity.this, ConductorHomeActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(true);
                            Intent intent = new Intent(StartTripActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }


                    }else{
                        new AlertDialog.Builder(StartTripActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setMessage("Driver Name "+response1.getName()+" \n Mobile number "+response1.getPhone()+" \n you can call him for more details")
                                .setPositiveButton("Call now", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CALENDAR)
                                                != PackageManager.PERMISSION_GRANTED) {
                                            // Permission is not granted
                                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                            callIntent.setData(Uri.parse("tel:" + response1.getPhone()));
                                            startActivity(callIntent);
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(StartTripActivity.this);
                }else{

                }

            }

            @Override
            public void onFailure(Call<in.limra.busdriver.response.loginresponse.Response> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location loc) {
        location = loc;
        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE, String.valueOf(loc.getLatitude()));
        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LONGITUDE, String.valueOf(loc.getLongitude()));

    }



    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(StartTripActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    location =                     LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(StartTripActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        location = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(StartTripActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }
    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(StartTripActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            gps = new GPSTracker(this);
            getMyLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(StartTripActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }
}
