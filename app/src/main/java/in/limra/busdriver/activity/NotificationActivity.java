package in.limra.busdriver.activity;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.adapter.SpinnerAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.data.SpinnerModel;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.batchresponse.BatchDetail;
import in.limra.busdriver.response.loginresponse.reasonresponse.NotificationReasonList;
import in.limra.busdriver.response.loginresponse.reasonresponse.ReasonResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    @BindView(R.id.reason)
    Spinner reason;

    @BindView(R.id.message)
    EditText message;

    @BindView(R.id.send)
    CardView send;

    SpinnerAdapter reasonAdapter;
    String reasonid;

    List<SpinnerModel> list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);


        list.clear();
        SpinnerModel selectModel=new SpinnerModel();
        selectModel.setId("0");
        selectModel.setName("Select Reason");
        list.add(selectModel);

        reasonAdapter=new SpinnerAdapter(this);
        reasonAdapter.setData(list);
        reason.setAdapter(reasonAdapter);

        reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(list.size()>position) {
                    reasonid = list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        getReason();
    }

    public void getReason(){
        Call<ReasonResponse> call= RestClient.get().getNotificationReasonList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ReasonResponse>() {
            @Override
            public void onResponse(Call<ReasonResponse> call, Response<ReasonResponse> response) {
                ReasonResponse reasonResponse=response.body();
                if(response.code()==200){
                    if(reasonResponse.getStatus())
                    for(int i=0;i<reasonResponse.getNotificationReasonList().size();i++){
                        NotificationReasonList notificationReasonList=reasonResponse.getNotificationReasonList().get(i);
                        SpinnerModel spinnerModel1=new SpinnerModel();
                        spinnerModel1.setId(notificationReasonList.getId());
                        spinnerModel1.setName(notificationReasonList.getName());

                        if(!list.contains(spinnerModel1)) {
                            list.add(spinnerModel1);
                        }
                    }
                    reasonAdapter.setData(list);
                    reason.setAdapter(reasonAdapter);
                }else if(response.code()==401){
                    AppUtils.logout(NotificationActivity.this);
                }else{

                }

            }

            @Override
            public void onFailure(Call<ReasonResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
