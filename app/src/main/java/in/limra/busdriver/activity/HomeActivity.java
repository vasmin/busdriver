package in.limra.busdriver.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.model.Step;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.AppMapView;
import in.limra.busdriver.Helper;
import in.limra.busdriver.LatLngInterpolator;
import in.limra.busdriver.MarkerAnimation;
import in.limra.busdriver.R;
import in.limra.busdriver.SingleShotLocationProvider;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.RealmHelper;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.Response;
import in.limra.busdriver.response.loginresponse.driverresponse.DriverResponse;
import in.limra.busdriver.response.loginresponse.maprouteresponse.RoutePickupPoint;
import in.limra.busdriver.response.loginresponse.maprouteresponse.RouteResponse;
import in.limra.busdriver.response.loginresponse.pointresponse.PointResponse;
import in.limra.busdriver.service.AlarmReceive;
import in.limra.busdriver.service.GPSTracker;
import in.limra.busdriver.service.LocationUpdatesIntentService;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;

import static in.limra.busdriver.MapUtils.getBearing;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback,  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    String ACTION_PROCESS_UPDATES =
            "com.google.android.gms.location.sample.backgroundlocationupdates.action" +
                    ".PROCESS_UPDATES";


    private AppBarConfiguration mAppBarConfiguration;
    TextView name;
    ProgressDialog progressBar;
    Realm realm;

    @BindView(R.id.mapView)
    AppMapView mapView;

    @BindView(R.id.stoptrip)
    TextView stopTrip;

    Marker busmarker,pointmarker;
    private SingleShotLocationProvider singleShotLocationProvider;
    double latitude=0;
    double longitude=0;
    boolean isTripStart=false;
    private GoogleMap gMap;
    private double  endLat, endLong;
    private ArrayList<PolylineOptions> polylineOptionList = new ArrayList<>();
    private List<RoutePickupPoint> list = new ArrayList<>();
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    private static final String TAG = HomeActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int REQUEST_BACKGROUND_PERMISSIONS_REQUEST_CODE = 34;
    private static final long UPDATE_INTERVAL =  1000;
    private static final long FASTEST_UPDATE_INTERVAL = 1000;
    private static final long MAX_WAIT_TIME = 1000;


    private GoogleApiClient mGoogleApiClient;
    // UI Widgets.
    private Button mRequestUpdatesButton;
    private Button mRemoveUpdatesButton;
    private TextView mLocationUpdatesResultView;


    //google Fused

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    boolean permissionAccessCoarseLocationApproved ;


    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    Location location;

    // GPSTracker gps;
    FirebaseDatabase userDB;
    DatabaseReference myRef;

    View view;
    /* For Google Fused API */
    private SettingsClient mSettingsClient;

    private static final int REQUEST_CHECK_SETTINGS = 214;
    private static final int REQUEST_ENABLE_GPS = 516;
    private final int REQUEST_LOCATION_PERMISSION = 214;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    @BindView(R.id.speed)
    TextView busSpeed;

    GPSTracker gpsTracker;


    // bus animation use
    private static final long DELAY = 4500;
    private static final long ANIMATION_TIME_PER_ROUTE = 3000;
    String polyLine = "q`epCakwfP_@EMvBEv@iSmBq@GeGg@}C]mBS{@KTiDRyCiBS";
    private PolylineOptions polylineOptions;
    private Polyline greyPolyLine;
    private SupportMapFragment mapFragment;
    private Handler handler;
    private int index;
    private int next;
    private LatLng startPosition;
    private LatLng endPosition;
    private float v;
    Button button2;
    List<LatLng> polyLineList;
    private double lat, lng;
    // Give your Server URL here >> where you get car location update
    public static final String URL_DRIVER_LOCATION_ON_RIDE = "*******";
    private boolean isFirstPosition = true;
    private Double startLatitude;
    private Double startLongitude;

    @SuppressLint({"ClickableViewAccessibility", "ShortAlarm"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppUtils.isNetworkConnectionAvailable(this);

        ButterKnife.bind(this);

        gpsTracker=new GPSTracker(this);



        userDB = FirebaseDatabase.getInstance();
        myRef = userDB.getReference(getResources().getString(R.string.app_name));
        myRef.setValue("");

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        //setUpGClient();
        realm = RealmHelper.getRealmInstance();

        // Check if the user revoked runtime permissions.

        if (alarmManager == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            }
            Intent intent = new Intent(this, AlarmReceive.class);
            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000,
                    pendingIntent);
        }

        stopTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTrip();
            }
        });


        if (!checkPermissions()) {
            requestPermissions();
        }else{
        }

        if(gpsTracker.canGetLocation()) {
            location = gpsTracker.getLocation();
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
               // Helper.showLog("Location Received");
                location = locationResult.getLastLocation();
                onLocationChanged(location);
            }
        };



        buildGoogleApiClient();



        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        Bundle extras = getIntent().getExtras();
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        mapView.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    //scrollView.requestDisallowInterceptTouchEvent(true);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    //scrollView.requestDisallowInterceptTouchEvent(false);
                    break;
            }
            return false;
        });

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        View header = navigationView.inflateHeaderView(R.layout.nav_header_home);
        name=header.findViewById(R.id.drivername);
        getDriverDetails();
        getPickupPointList();
        getRoutePointsList();

    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // Helper.showLog("Location Received");
                location = locationResult.getLastLocation();
                onLocationChanged(location);
            }
        };
        createLocationRequest();
    }

    @Override
    public void onPause() {
        super.onPause();

        stopLocationUpdates();
        mGoogleApiClient.stopAutoManage(HomeActivity.this);
        mGoogleApiClient.disconnect();
        mapView.onPause();
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(locationCallback);
    }
    public void endTrip(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
        Call<Response> call=RestClient.get().endTrip(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setTripStart(false);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(false);
                        AppUtils.snackBar(HomeActivity.this,response1.getMessage());
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.deleteAll();
                            }
                        });

                        myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).removeValue();

                        Intent intent=new Intent(HomeActivity.this,StartTripActivity.class);
                        startActivity(intent);
                        finish();


                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.TRIP_ID,"0");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BUS_ID,"0");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BATCH_ID,"0");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.ROUTE_ID,"0");

                    }else{
                        AppUtils.errorSnackBar(HomeActivity.this,response1.getMessage());
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{
                    AppUtils.errorSnackBar(HomeActivity.this,Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(HomeActivity.this,Constants.SERVERTIMEOUT);
                progressBar.dismiss();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        Log.e("onResume","onResume");
        if(mFusedLocationClient!=null) {
            mFusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    Looper.getMainLooper());
            AppUtils.isNetworkConnectionAvailable(this);
        }
    }



    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(HomeActivity.this);
            mGoogleApiClient.disconnect();
        }
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setSmallestDisplacement(10f);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }


    public void getDriverDetails(){
        progressBar.show();
        Call<DriverResponse> call=RestClient.get().getDriverDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<DriverResponse>() {
            @Override
            public void onResponse(Call<DriverResponse> call, retrofit2.Response<DriverResponse> response) {
                progressBar.dismiss();
                DriverResponse driverResponse=response.body();
                if(response.code()==200){
                    if(driverResponse.getStatus()){
                        name.setText(driverResponse.getDriverDetails().getName());
                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{
                    AppUtils.errorSnackBar(HomeActivity.this,Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<DriverResponse> call, Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
                AppUtils.errorSnackBar(HomeActivity.this,Constants.SERVERTIMEOUT);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to close this Application?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            if (!SharedPreferenceHelper.getInstance(getApplicationContext()).isTripStart()) {
                logout();
            } else {
                AppUtils.errorSnackBar(HomeActivity.this, "Please stop trip before logout ");
            }
            return true;
        }


        if (id == R.id.stop) {
           Intent intent=new Intent(HomeActivity.this,ConductorHomeActivity.class);
           startActivity(intent);
        }

        if (id == R.id.notification) {
            Intent intent = new Intent(HomeActivity.this, NotificationActivity.class);
            startActivity(intent);
        }

        if (id == R.id.help) {
            Intent intent = new Intent(HomeActivity.this, HelpActivity.class);
            startActivity(intent);
        }


        return false;
    }

    public void logout() {

        HashMap<String, String> hashMap = new HashMap<>();
        Call<Response> call = RestClient.get().logout(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(), hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1 = response.body();
                if (response.code() == 200) {
                    if (response1.getStatus()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.deleteAll();
                            }
                        });
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(false);
                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        AppUtils.errorSnackBar(HomeActivity.this, response1.getMessage());
                    }
                } else {
                    AppUtils.errorSnackBar(HomeActivity.this, Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(HomeActivity.this, Constants.SERVERTIMEOUT);
            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            if(!SharedPreferenceHelper.getInstance(getApplicationContext()).isTripStart()) {
                AppUtils.logout(HomeActivity.this);
            }else{
                AppUtils.errorSnackBar(HomeActivity.this,"Please stop trip before logout ");
            }
            return true;
        }

        if (item.getItemId() == R.id.nav_home) {
        }

        return super.onOptionsItemSelected(item);
    }





    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {



        gMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        gMap.getUiSettings().setMapToolbarEnabled(true);
        gMap.getUiSettings().setZoomControlsEnabled(true);
        gMap.setIndoorEnabled(false);

        gMap.animateCamera( CameraUpdateFactory.zoomTo( 25f ) );

        LatLng ny = new LatLng(latitude, longitude);
        gMap.moveCamera(CameraUpdateFactory.newLatLng(ny));
        Drawable bus = getResources().getDrawable(R.drawable.bus);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(bus);
        if (busmarker != null) {
            busmarker.remove();
        }

        busmarker = gMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Your Here").anchor(0.5f, 0.5f).icon(markerIcon).flat(true));

        //  address = AppUtils.getCompleteAddressString(getApplicationContext(),latitude, longitude);
        // addressTextView.setText(address);
        Date c = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("EEEE, dd/MM/yyyy hh:mm:ss aa");
        String formattedDate = df.format(c);
        String date = new Date().toString();
        //timeStampTextView.setText(date.substring(0,date.indexOf("GMT")-1));
        // timeStampTextView.setText(formattedDate);
        //  latlng.setText("LAT :"+latitude +" LONG :"+longitude);

        //  gMap = googleMap;
        // LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));

        //getRoutePointsList();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }
        mapView.onSaveInstanceState(mapViewBundle);
    }





    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }





    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onLocationChanged(Location loc) {
        int suitableMeter = 20;
        if (location.hasAccuracy()  && location.getAccuracy() <= suitableMeter) {
            location = loc;
            latitude=location.getLatitude();
            longitude=location.getLongitude();
            singleShotLocationProvider = new SingleShotLocationProvider(getApplicationContext());
            Toast.makeText(this, "Location change call", Toast.LENGTH_SHORT).show();

            if(SharedPreferenceHelper.getInstance(getApplicationContext()).isTracking()) {

                Location local=singleShotLocationProvider.getLoc();
                if(local!=null) {
                    latitude=local.getLatitude();
                    longitude=local.getLongitude();

                    //String address = AppUtils.getCompleteAddressString(getApplicationContext(), local.getLatitude(), local.getLongitude());

                    int speed = (int) ((local.getSpeed() * 3600) / 1000);
                    Toast.makeText(getApplicationContext(), "Your speeed" + String.valueOf(speed + " km/h"), Toast.LENGTH_LONG).show();
                    busSpeed.setText("Bus speed "+speed+ " km/h");
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(local.getLatitude()));
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(local.getLongitude()));
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                   // myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("address").setValue(address);



                }else{
                  //  String address = AppUtils.getCompleteAddressString(getApplicationContext(), location.getLatitude(), location.getLongitude());
                    int speed = (int) ((location.getSpeed() * 3600) / 1000);
                    Toast.makeText(getApplicationContext(), "Your speeed" + String.valueOf(speed + " km/h"), Toast.LENGTH_LONG).show();
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(location.getLatitude()));
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(location.getLongitude()));
                    myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
                  //  myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("address").setValue(address);
                    busSpeed.setText("Bus speed "+speed+ " km/h");
                }

            }

            if(gMap!=null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        startLatitude = location.getLatitude();
                        startLongitude = location.getLongitude();

                        Log.d(TAG, startLatitude + "--" + startLongitude);


                        if (isFirstPosition) {
                            if(busmarker!=null){
                                busmarker.remove();
                            }

                            startPosition = new LatLng(startLatitude, startLongitude);
                            Drawable bus = getResources().getDrawable(R.drawable.bus);
                            BitmapDescriptor markerIcon = getMarkerIconFromDrawable(bus);
                            busmarker = gMap.addMarker(new MarkerOptions().position(startPosition).
                                    flat(true).icon(markerIcon));
                            busmarker.setAnchor(0.5f, 0.5f);

                            gMap.moveCamera(CameraUpdateFactory
                                    .newCameraPosition
                                            (new CameraPosition.Builder()
                                                    .target(startPosition)
                                                    .zoom(25)
                                                    .build()));

                            isFirstPosition = false;

                        } else {
                            endPosition = new LatLng(startLatitude, startLongitude);
                            Log.d(TAG, startPosition.latitude + "--" + endPosition.latitude + "--Check --" + startPosition.longitude + "--" + endPosition.longitude);

                            if ((startPosition.latitude != endPosition.latitude) || (startPosition.longitude != endPosition.longitude)) {
                                Log.e(TAG, "NOT SAME");
                                startBikeAnimation(startPosition, endPosition);
                            } else {
                                Log.e(TAG, "SAMME");
                            }
                        }




                       /* //gMap.clear();

                        // routeDraw(list);
                        if(busmarker!=null){
                            busmarker.remove();
                        }

                        gMap.getUiSettings().setMapToolbarEnabled(true);
                        gMap.getUiSettings().setZoomControlsEnabled(true);
                        gMap.setIndoorEnabled(false);
                        gMap.animateCamera( CameraUpdateFactory.zoomTo( 25f ) );
                        *//*
                        gMap.getUiSettings().setMapToolbarEnabled(true);
                        gMap.getUiSettings().setZoomGesturesEnabled(true);
                        gMap.getUiSettings().setZoomControlsEnabled(true);
                        gMap.setIndoorEnabled(false);*//*
                        LatLng ny = new LatLng(latitude, longitude);
                        gMap.moveCamera(CameraUpdateFactory.newLatLng(ny));
                        Drawable circleDrawable = getResources().getDrawable(R.drawable.bus);
                        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
                        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Your Here");
                        marker.anchor(0.5f, 0.5f);
                        marker.icon(markerIcon);
                        // marker.rotation((float) brng);
                        marker.flat(true);
                        busmarker=gMap.addMarker(marker);
                  *//*  gMap.moveCamera(CameraUpdateFactory.newLatLng(ny));
                    gMap.animateCamera(CameraUpdateFactory.zoomTo(20));*//*

                        //refreshMapPosition(ny,45);
                        showMarker(ny);*/
                        Log.e("Bus Map","Marker set");
                        busmarker.setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
                        busmarker.setRotation(location.getBearing());
                    }
                });
            }
        }

    }

    private void startBikeAnimation(final LatLng start, final LatLng end) {

        Log.i(TAG, "startBikeAnimation called...");

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(ANIMATION_TIME_PER_ROUTE);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                //LogMe.i(TAG, "Car Animation Started...");
                v = valueAnimator.getAnimatedFraction();
                lng = v * end.longitude + (1 - v)
                        * start.longitude;
                lat = v * end.latitude + (1 - v)
                        * start.latitude;

                LatLng newPos = new LatLng(lat, lng);
                busmarker.setPosition(newPos);
                busmarker.setAnchor(0.5f, 0.5f);
                busmarker.setRotation(getBearing(start, end));

                // todo : Shihab > i can delay here
                gMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition
                                (new CameraPosition.Builder()
                                        .target(newPos)
                                        .zoom(25)
                                        .bearing(45)
                                        .build()));
                startPosition = busmarker.getPosition();
            }
        });
        valueAnimator.start();
    }

    private void showMarker(@NonNull LatLng latLng) {

        if (busmarker == null) {
            Drawable circleDrawable = getResources().getDrawable(R.drawable.bus);
            BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
            busmarker = gMap.addMarker(new MarkerOptions().icon(markerIcon).position(latLng));
        }else {
            gMap.animateCamera( CameraUpdateFactory.zoomTo( 25f ) );
            MarkerAnimation.animateMarkerToGB(busmarker, latLng, new LatLngInterpolator.Spherical());
          /*  CameraPosition.Builder positionBuilder = new CameraPosition.Builder();
            positionBuilder.target(latLng);
            positionBuilder.zoom(25);
            positionBuilder.bearing(45);
            positionBuilder.tilt(60);
            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(positionBuilder.build()));*/
        }
    }


        private void refreshMapPosition(LatLng pos, float angle) {
        CameraPosition.Builder positionBuilder = new CameraPosition.Builder();
        positionBuilder.target(pos);
        positionBuilder.zoom(25);
        positionBuilder.bearing(angle);
        positionBuilder.tilt(60);
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(positionBuilder.build()));
    }


    public void getRoutePointsList(){

        HashMap<String,String > hashMap=new HashMap<>();
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
        Call<RouteResponse> call=RestClient.get().getRoutePointsList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<RouteResponse>() {
            @Override
            public void onResponse(Call<RouteResponse> call, retrofit2.Response<RouteResponse> response) {
                RouteResponse routeResponse=response.body();
                if(response.code()==200){
                    if(routeResponse.getStatus()){
                        list.clear();
                        list.addAll(routeResponse.getRoutePickupPoints());
                        routeDraw(list);
                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(Call<RouteResponse> call, Throwable t) {

            }
        });
    }
    public void routeDraw(final List<RoutePickupPoint> list){
        try {
            LatLng lastLatLang = null;
            final ArrayList<LatLng> middleLatLang = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (list.size() - 1 == i) {
                    lastLatLang = new LatLng(Double.parseDouble(list.get(i).getLatitude()), Double.parseDouble(list.get(i).getLongitude()));

                    endLat = Double.parseDouble(list.get(i).getLatitude());
                    endLong = Double.parseDouble(list.get(i).getLongitude());

                } else {

                    middleLatLang.add(new LatLng(Double.parseDouble(list.get(i).getLatitude()), Double.parseDouble(list.get(i).getLongitude())));
                }
            }
            GoogleDirection.withServerKey(getResources().getString(R.string.google_maps_key))
                    .from(new LatLng(latitude,
                            longitude))
                    .and(middleLatLang)
                    .to(lastLatLang)
                    .avoid(AvoidType.FERRIES)
                    .avoid(AvoidType.HIGHWAYS)
                    .alternativeRoute(true)
                    .execute(new DirectionCallback() {
                        @Override
                        public void onDirectionSuccess(Direction direction, String rawBody) {
                            String status = direction.getStatus();
                            Log.e("Raw Body",rawBody);
                            Log.e("Direction", status);
                            if (direction.isOK()) {
                                Route route = direction.getRouteList().get(0);
                                int legCount = route.getLegList().size();
                                for (int index = 0; index < legCount; index++) {
                                    Leg leg = route.getLegList().get(index);

                                    gMap.addMarker(new MarkerOptions().position(leg.getStartLocation()
                                            .getCoordination())
                                            .icon(AppUtils.bitmapDescriptorFromVector(getApplicationContext(), R.drawable.busstop))
                                            .anchor(0.5f, 1)
                                            .snippet(list.get(index).getStop())
                                            .title(list.get(index).getStopName()))
                                            .setTag(list.get(index).getPointId());

                                    if (index == legCount - 1) {

                                        gMap.addMarker(new MarkerOptions().position(leg.getEndLocation().getCoordination())
                                                .icon(AppUtils.bitmapDescriptorFromVector(getApplicationContext(), R.drawable.busstop))
                                                .title(list.get(index).getStopName()))
                                                .setTag(list.get(index).getPointId());

                                    }
                                    List<Step> stepList = leg.getStepList();
                                    polylineOptionList = DirectionConverter.createTransitPolyline(getApplicationContext(), stepList, 5, Color.parseColor("#8BC1EB"), 3, Color.GREEN);

                                    for (PolylineOptions polylineOption : polylineOptionList) {
                                        gMap.addPolyline(polylineOption);
                                    }

                                }

                                Location loc1 = new Location("");
                                loc1.setLatitude(latitude);
                                loc1.setLongitude(longitude);

                                Location loc2 = new Location("");
                                loc2.setLatitude(endLat);
                                loc2.setLongitude(endLong);

                                //Calculate distance

                        /*    distanceInMeters += loc1.distanceTo(loc2)/1000;
                            String distance="0";
                            if (distanceInMeters > 0.999){
                                distance  = String.valueOf(distanceInMeters) + "km";
                            }else {
                                distance = String.valueOf(distanceInMeters) + "m";
                            }

                            total_current_distance.setText(distance);*/

                           /* gMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                                @Override
                                public View getInfoWindow(Marker marker) {
                                   *//* View v = getActivity().getLayoutInflater().inflate(R.layout.marker_info_windows, null);
                                    // MyTextView stopNameText = v.findViewById(R.id.titleName);
                                    MyTextView totalStudent = v.findViewById(R.id.totalStudent);
                                    // stopNameText.setText(marker.getTitle());

                                    current_address.setText(marker.getTitle());
                                    totalStudent.setText(marker.getSnippet());*//*
                                    return v;

                                }

                                @Override
                                public View getInfoContents(Marker marker) {
                                    return null;
                                }
                            });*/


                                setCameraWithCoordinationBounds(route);
                            } else {
                                // Do something
                            }
                        }

                        @Override
                        public void onDirectionFailure(Throwable t) {

                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
            AppUtils.errorSnackBar(HomeActivity.this,"Sorry No point Found for these trip");
        }

    }

    private void setCameraWithCoordinationBounds(Route route) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        gMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdate() {
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }


    @Override
    public void onConnected(Bundle bundle) {

        Log.i(TAG, "GoogleApiClient connected");
        requestLocationUpdates();

        checkPermissions();
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        routeDraw(list);
    }
    private PendingIntent getPendingIntent() {
        Intent intent = new Intent(this, LocationUpdatesIntentService.class);
        intent.setAction(ACTION_PROCESS_UPDATES);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }else{
            return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
    }

    public void requestLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, locationRequest, getPendingIntent());

        } catch (SecurityException e) {
            e.printStackTrace();
        }

        mFusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.getMainLooper());
    }

    @Override
    public void onConnectionSuspended(int i) {
        connectGoogleClient();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private void connectGoogleClient() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SUCCESS) {
            googleApiClient.connect();
        } else {
            int REQUEST_GOOGLE_PLAY_SERVICE = 988;
            googleAPI.getErrorDialog(this, resultCode, REQUEST_GOOGLE_PLAY_SERVICE);
        }
    }

    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(HomeActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(HomeActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        location = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(HomeActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        // gps=new GPSTracker(ConductorHomeActivity.this);
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }


        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    requestLocationUpdate();
                    break;

            }
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
           /* Snackbar.make(
                    findViewById(R.id.activity_main),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();*/
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(HomeActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
        permissionAccessCoarseLocationApproved= ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;

        if (permissionAccessCoarseLocationApproved) {
            boolean backgroundLocationPermissionApproved =
                    ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                            == PackageManager.PERMISSION_GRANTED;

            if (backgroundLocationPermissionApproved) {
                // App can access location both in the foreground and in the background.
                // Start your service that doesn't have a foreground service type
                // defined.
            } else {
                // App can only access location in the foreground. Display a dialog
                // warning the user that your app must have all-the-time access to
                // location in order to function properly. Then, request background
                // location.
                ActivityCompat.requestPermissions(this, new String[] {
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                        REQUEST_BACKGROUND_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    },
                    REQUEST_BACKGROUND_PERMISSIONS_REQUEST_CODE);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
            // startRepeatingTimer();
            //gps = new GPSTracker(ConductorHomeActivity.this);
        }
    }



    @Override
    protected void onDestroy() {
        //Remove location update callback here

        mapView.onDestroy();

        super.onDestroy();
    }

    private void getPickupPointList(){
        HashMap<String,String> hashMap=new HashMap<>();
        if(location!=null) {
            hashMap.put("lat", String.valueOf(latitude));
            hashMap.put("long", String.valueOf(longitude));
        }
        hashMap.put("type", "pick");
        hashMap.put("route_id", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.ROUTE_ID));
        hashMap.put("batch_id", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.BATCH_ID));
        hashMap.put("trip_id", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));

        Call<PointResponse> call= RestClient.get().getPickupPointList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PointResponse>() {
            @Override
            public void onResponse(Call<PointResponse> call, retrofit2.Response<PointResponse> response) {
                PointResponse pointResponse=response.body();
                if(response.code()==200){
                    if(pointResponse.getStatus()){
                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{
                    AppUtils.errorSnackBar(HomeActivity.this,Constants.SOMETHING_WENT_WRONG);

                }
            }

            @Override
            public void onFailure(Call<PointResponse> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(HomeActivity.this,Constants.SERVERTIMEOUT);
            }
        });
    }
}
