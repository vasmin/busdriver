package in.limra.busdriver.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.limra.busdriver.R;
import in.limra.busdriver.adapter.ViewPagerAdapter.ViewPagerAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.RealmHelper;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.fragment.StudentListFragment;
import in.limra.busdriver.fragment.StopListFragment;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.Response;
import in.limra.busdriver.response.loginresponse.driverresponse.DriverResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;

public class ConductorHomeActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, SwipeRefreshLayout.OnRefreshListener,NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.switchForActionBar)
    Switch aSwitch;

    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    Location location;

   // GPSTracker gps;
    Realm realm;
    ProgressDialog progressBar;

    private AppBarConfiguration mAppBarConfiguration;
    TextView name;
    FirebaseDatabase userDB;
    DatabaseReference myRef;

    View view;
    /* For Google Fused API */
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private SettingsClient mSettingsClient;

    private static final int REQUEST_CHECK_SETTINGS = 214;
    private static final int REQUEST_ENABLE_GPS = 516;
    private final int REQUEST_LOCATION_PERMISSION = 214;

    @BindView(R.id.accounttype)
    TextView accountType;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conductor_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AppUtils.isNetworkConnectionAvailable(this);

        ButterKnife.bind(this);

        userDB = FirebaseDatabase.getInstance();
        myRef = userDB.getReference(getResources().getString(R.string.app_name));
        myRef.setValue("");

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        setUpGClient();
        realm = RealmHelper.getRealmInstance();
        initTab();
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(ConductorHomeActivity.this);

        View header = navigationView.inflateHeaderView(R.layout.nav_header_home);
        name=header.findViewById(R.id.drivername);

        getDriverDetails();



        if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.ACCOUNT_TYPE)!=null)
        if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.ACCOUNT_TYPE).equals("Conductor")){
            aSwitch.setVisibility(View.VISIBLE);
            accountType.setText("Conductor");
            checkBusTracking();
        }else{
            aSwitch.setVisibility(View.GONE);
            accountType.setText("Driver ");
        }

        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(aSwitch.isChecked()){
                    new AlertDialog.Builder(ConductorHomeActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Start-Tracking")
                            .setCancelable(false)
                            .setMessage("Are you sure you want to start tracking?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    changeTrackingStatus();
                                }

                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    aSwitch.setChecked(false);
                                }

                            })
                            .show();

                }else{
                    new AlertDialog.Builder(ConductorHomeActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Stop Tracking")
                            .setCancelable(false)
                            .setMessage("Are you sure you want to close tracking?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    changeTrackingStatus();
                                }

                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    aSwitch.setChecked(true);
                                }
                            })
                            .show();
                }
            }
        });

/*
        if (permissionAccessCoarseLocationApproved) {
            boolean backgroundLocationPermissionApproved =
                    ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                            == PackageManager.PERMISSION_GRANTED;

            if (backgroundLocationPermissionApproved) {
                // App can access location both in the foreground and in the background.
                // Start your service that doesn't have a foreground service type
                // defined.
            } else {
                // App can only access location in the foreground. Display a dialog
                // warning the user that your app must have all-the-time access to
                // location in order to function properly. Then, request background
                // location.*/
                ActivityCompat.requestPermissions(this, new String[] {
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                        REQUEST_LOCATION_PERMISSION);
           /* }
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    },
                    REQUEST_LOCATION_PERMISSION);
        }*/

    }


    protected synchronized void buildGoogleApiClient() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        connectGoogleClient();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
               // Helper.showLog("Location Received");
                mCurrentLocation = locationResult.getLastLocation();
                onLocationChanged(mCurrentLocation);
            }
        };
    }

    private void connectGoogleClient() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SUCCESS) {
            googleApiClient.connect();
        } else {
            int REQUEST_GOOGLE_PLAY_SERVICE = 988;
            googleAPI.getErrorDialog(this, resultCode, REQUEST_GOOGLE_PLAY_SERVICE);
        }
    }


    public void getDriverDetails(){
        progressBar.show();
        Call<DriverResponse> call=RestClient.get().getDriverDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<DriverResponse>() {
            @Override
            public void onResponse(Call<DriverResponse> call, retrofit2.Response<DriverResponse> response) {
                progressBar.dismiss();
                DriverResponse driverResponse=response.body();
                Log.d("fygyrg","fgjjfg"+driverResponse);
                Log.d("fygyrg","fgjjfg"+response.body());
                if(response.code()==200){
                    if(driverResponse.getStatus()){
                        name.setText(driverResponse.getDriverDetails().getName());
                    }else
                        {

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ConductorHomeActivity.this);
                }else{
                    AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<DriverResponse> call, Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
                AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SERVERTIMEOUT);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initTab() {

        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setupTabIcons() {
        String[] name = {"Stop List", "Student List"};
        Objects.requireNonNull(tabLayout.getTabAt(0)).setText(name[0]);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setText(name[1]);
//        Objects.requireNonNull(tabLayout.getTabAt(2)).setText(name[2]);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("edttext", "From Activity");

        adapter.addFrag(new StopListFragment(), "Stop List");
        // adapter.addFrag(new TripDropListFragment(), "Drop List");
        adapter.addFrag(new StudentListFragment(), "Student List");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    private void startLocationUpdates() {
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback,
                    Looper.getMainLooper());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        if(mLocationCallback!=null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        MenuItem stoptrip = menu.findItem(R.id.stoptrip);
        if(SharedPreferenceHelper.getInstance(getApplicationContext()).isTripStart()) {
            stoptrip.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.stoptrip) {
            endTrip();

        }


        return super.onOptionsItemSelected(item);
    }

    public void logout() {

        HashMap<String, String> hashMap = new HashMap<>();
        Call<Response> call = RestClient.get().logout(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(), hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1 = response.body();
                if (response.code() == 200) {
                    if (response1.getStatus()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.deleteAll();
                            }
                        });
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(false);
                        Intent intent = new Intent(ConductorHomeActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        AppUtils.errorSnackBar(ConductorHomeActivity.this, response1.getMessage());
                    }
                } else {
                    AppUtils.errorSnackBar(ConductorHomeActivity.this, Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(ConductorHomeActivity.this, Constants.SERVERTIMEOUT);
            }
        });
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location loc) {
        location = loc;

        String latitude = String.valueOf(location.getLatitude());
        String longitude = String.valueOf(location.getLongitude());

        if (latitude.equalsIgnoreCase("0.0") && longitude.equalsIgnoreCase("0.0")) {
            requestLocationUpdate();
        } else {
            //Perform Your Task with LatLong
        }


        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE, String.valueOf(loc.getLatitude()));
        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LONGITUDE, String.valueOf(loc.getLongitude()));

        if(SharedPreferenceHelper.getInstance(getApplicationContext()).isTracking()) {
            int speed = (int) ((location.getSpeed() * 3600) / 1000);
            Toast.makeText(getApplicationContext(), "Your speeed" + String.valueOf(speed + " km/h"), Toast.LENGTH_LONG).show();


            //String address = AppUtils.getCompleteAddressString(getApplicationContext(), loc.getLatitude(), loc.getLongitude());
            myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
            myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("Latitude").setValue(String.valueOf(loc.getLatitude()));
            myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("Longitude").setValue(String.valueOf(loc.getLongitude()));
            myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("speed").setValue(String.valueOf(speed));
           // myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).child("address").setValue(address);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdate() {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }


    @Override
    public void onConnected(Bundle bundle) {

        checkPermissions();
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        connectGoogleClient();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }



    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(ConductorHomeActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(ConductorHomeActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        location = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(ConductorHomeActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                       // gps=new GPSTracker(ConductorHomeActivity.this);
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }


        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    requestLocationUpdate();
                    break;

            }
        }
    }

    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(ConductorHomeActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            listPermissionsNeeded.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } else {
            //gps = new GPSTracker(ConductorHomeActivity.this);
            getMyLocation();

          //  startRepeatingTimer();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(ConductorHomeActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
           // startRepeatingTimer();
            //gps = new GPSTracker(ConductorHomeActivity.this);
        }
    }



    @Override
    protected void onDestroy() {
        //Remove location update callback here
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        getDriverDetails();
    }

    public void changeTrackingStatus(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
        if(SharedPreferenceHelper.getInstance(getApplicationContext()).isTracking()) {
            hashMap.put("tracking", "Off");
        }else{
            hashMap.put("tracking", "On");
        }
        Call<Response> call=RestClient.get().changeBusTracking(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        if(response1.getTracking_status().equals("On")){
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(true);
                        }else{
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(false);
                        }
                    }else{
                        AppUtils.errorSnackBar(ConductorHomeActivity.this,response1.getMessage());
                    }

                }else if(response.code()==401)
                {
                    AppUtils.logout(ConductorHomeActivity.this);
                }else{
                    AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SERVERTIMEOUT);
                progressBar.dismiss();
            }
        });
    }

    public void checkBusTracking(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
        Call<Response> call=RestClient.get().checkBusTracking(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){

                    if (response1.getStatus()) {

                        if (response1.getTracking_status().equals("On")) {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(true);
                            aSwitch.setChecked(true);
                        } else {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(false);
                            aSwitch.setChecked(false);
                        }
                    }else {
                        AppUtils.errorSnackBar(ConductorHomeActivity.this,response1.getMessage());
                    }

                }else if(response.code()==401){
                    AppUtils.logout(ConductorHomeActivity.this);

                }else{
                    AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
                AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SERVERTIMEOUT);

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.ACCOUNT_TYPE)!=null)
                if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.ACCOUNT_TYPE).equals("Conductor")) {

                    new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Exit")
                            .setMessage("Are you sure you want to close this Application?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();

                }else{
                    super.onBackPressed();
                }

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            if (!SharedPreferenceHelper.getInstance(getApplicationContext()).isTripStart()) {
                logout();
            } else {
                AppUtils.errorSnackBar(ConductorHomeActivity.this, "Please stop trip before logout ");
            }
            return true;
        }
        if (id == R.id.stoptrip) {
            endTrip();

        }

        if (id == R.id.notification) {
            Intent intent = new Intent(ConductorHomeActivity.this, NotificationActivity.class);
            startActivity(intent);
        }

        if (id == R.id.help) {
            Intent intent = new Intent(ConductorHomeActivity.this, HelpActivity.class);
            startActivity(intent);
        }


        return false;
    }





    public void endTrip(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("trip_id",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID));
        Call<Response> call=RestClient.get().endTrip(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setTripStart(false);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setTracking(false);
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.TRIP_ID,"0");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BUS_ID,"0");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.BATCH_ID,"0");
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.ROUTE_ID,"0");
                        AppUtils.snackBar(ConductorHomeActivity.this,response1.getMessage());
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.deleteAll();
                            }
                        });

                        if(myRef!=null && SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)!=null)
                        myRef.child(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.TRIP_ID)).removeValue();


                        Intent intent=new Intent(ConductorHomeActivity.this,StartTripActivity.class);
                        startActivity(intent);
                        finish();

                    }else{
                        AppUtils.errorSnackBar(ConductorHomeActivity.this,response1.getMessage());
                    }
                }else if(response.code()==401){
                    AppUtils.logout(ConductorHomeActivity.this);
                }else{
                    AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                AppUtils.errorSnackBar(ConductorHomeActivity.this,Constants.SERVERTIMEOUT);
                progressBar.dismiss();
            }
        });
    }
}



