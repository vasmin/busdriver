package in.limra.busdriver.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.View;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.adapter.ViewPagerAdapter.ViewPagerAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.fragment.AssignStudentListFragment;
import in.limra.busdriver.fragment.StopListFragment;
import in.limra.busdriver.fragment.StudentListFragment;
import in.limra.busdriver.fragment.UnassignStudentListFragment;

public class PointListActivity extends AppCompatActivity {


    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    String stopname="";
    String pointid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            stopname = extras.getString(Constants.STOPNAME);
            pointid = extras.getString(Constants.POINT_ID);
            getSupportActionBar().setTitle(stopname);
            initTab();
            //The key argument here must match that used in the other activity
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initTab() {
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setupTabIcons() {
        String[] name = {"Assign student list", "Unassign student list"};
        Objects.requireNonNull(tabLayout.getTabAt(0)).setText(name[0]);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setText(name[1]);
//        Objects.requireNonNull(tabLayout.getTabAt(2)).setText(name[2]);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("edttext", "From Activity");

        adapter.addFrag(AssignStudentListFragment.newInstance(stopname,pointid), "Assign student list");
        adapter.addFrag(UnassignStudentListFragment.newInstance(stopname,pointid), "Unassign student list");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
