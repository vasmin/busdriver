package in.limra.busdriver.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.view.WindowManager;

import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.SharedPreferenceHelper;

public class SplashActivity extends AppCompatActivity {
    private static final long SPLASH_TIME_OUT = 4000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
        AppUtils.checkAndRequestPermissions(this);
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.VIBRATE,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CAMERA,Manifest.permission.READ_PHONE_STATE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                        123);
            }
        }

        getWindow().setBackgroundDrawableResource(R.drawable.splash_bg);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
//******change activity here*******
        setAnimation();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if(SharedPreferenceHelper.getInstance(getApplicationContext()).isLoggerIn()){
                    if(SharedPreferenceHelper.getInstance(getApplicationContext()).isTripStart()){

                        if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.ACCOUNT_TYPE).equals("Conductor")) {
                            Intent intent = new Intent(SplashActivity.this, ConductorHomeActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    }else{
                        Intent i = new Intent(SplashActivity.this, StartTripActivity.class);
                        startActivity(i);
                        finish();
                    }

                }
                else{
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void setAnimation() {

        @SuppressLint("CutPasteId") ObjectAnimator fadeOut = ObjectAnimator.ofFloat(findViewById(R.id.logo), "alpha",  1f, .3f);
        fadeOut.setDuration(2000);
        @SuppressLint("CutPasteId") ObjectAnimator fadeIn = ObjectAnimator.ofFloat(findViewById(R.id.logo), "alpha", .3f, 1f);
        fadeIn.setDuration(2000);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn).after(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
            }
        });
        mAnimationSet.start();
    }
}
