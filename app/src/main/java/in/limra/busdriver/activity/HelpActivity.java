package in.limra.busdriver.activity;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.limra.busdriver.R;
import in.limra.busdriver.adapter.HelpAdapter;
import in.limra.busdriver.adapter.SpinnerAdapter;
import in.limra.busdriver.adapter.TripAdapter;
import in.limra.busdriver.data.AppUtils;
import in.limra.busdriver.data.Constants;
import in.limra.busdriver.data.SharedPreferenceHelper;
import in.limra.busdriver.network.RestClient;
import in.limra.busdriver.response.loginresponse.helpresponse.HelpResponse;
import in.limra.busdriver.response.loginresponse.pointresponse.PickupPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    HelpAdapter adapter;

    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);



        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new HelpAdapter(this, new HelpAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(PickupPoint d, View view) throws ParseException {

            }
        });

        getHelpList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getHelpList(){
        swipeRefreshLayout.setRefreshing(true);
        Call<HelpResponse> call= RestClient.get().getHelpConatctDetailsList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<HelpResponse>() {
            @Override
            public void onResponse(Call<HelpResponse> call, Response<HelpResponse> response) {
                HelpResponse helpResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(helpResponse.getStatus()){
                        adapter.setData(helpResponse.getHelpContactList());
                        recyclerView.setAdapter(adapter);
                    }else{
                        AppUtils.errorSnackBar(HelpActivity.this,Constants.SOMETHING_WENT_WRONG);
                    }


                }else if(response.code()==401){
                    AppUtils.logout(HelpActivity.this);
                }else{
                    AppUtils.errorSnackBar(HelpActivity.this, Constants.SOMETHING_WENT_WRONG);
                }

            }

            @Override
            public void onFailure(Call<HelpResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                AppUtils.errorSnackBar(HelpActivity.this, Constants.SERVERTIMEOUT);

            }
        });
    }

    @Override
    public void onRefresh() {
        getHelpList();
    }
}
